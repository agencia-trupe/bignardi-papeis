/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	//config.extraPlugins = 'font,colorbutton';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [];

	config.toolbar = [
	    [ 'Bold', 'Italic', 'Underline' ],
	    [ 'NumberedList', 'BulletedList' ],
	    [ 'Link', 'Unlink' ],
	    [ 'FontSize', 'TextColor' ],
	    [ 'InjectImage' ]
	];

	config.fontSize_sizes = '11/11px;12/12px;13/13px;14/14px;15/15px;16/16px;17/17px;18/18px;19/19px;20/20px;21/21px;22/22px;23/23px;24/24px;';
	config.colorButton_colors = '#000/000,#FFF/FFF,#00587A/00587A,#52CDDD/52CDDD';
	
	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
