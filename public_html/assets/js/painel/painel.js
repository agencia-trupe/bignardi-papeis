jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function filtraCidades(id_estado, target){
    $.getJSON('ajax/pegarCidades/'+id_estado, function(resposta){
        target.html("<option value=''>Carregando Cidades...</option>");
        var itens = "<option value=''>Todas as Cidades do Estado</option>";
        resposta.map( function(a, b){
            itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
        });
        target.html(itens);
        if(target.attr('data-cidade')){
            target.attr('data-cidade') == 0 ? target.val() : target.val(target.attr('data-cidade'));
        }
    });
}

function bindSelectCidades(){
    $('.seleciona_estado').on('change', function(){
        var id_estado = $(this).val();
        if(id_estado)
            filtraCidades(id_estado, $(this).parent().next().find('.seleciona_cidade'));
    });
    $('.btn-remover').on('click', function(e){
        e.preventDefault();
        $(this).parent().prev().remove();
        $(this).parent().prev().remove();
        $(this).parent().remove();
    });    
}

$('document').ready( function(){

    $('#add-cor').click( function(e){e.preventDefault()});

    $('#submit-cor').click( function(e){
        e.preventDefault();
        var tit = $('#add-cor-tit').val();
        var hex = $('#add-cor-hex').val();
        $.post('ajax/adicionarCor', {
            tit : tit,
            hex : hex
        }, function(retorno){
            retornojson = JSON.parse(retorno);
            if(retornojson.erro == 1){
                alert(retornojson.mensagem);
            }else{
                $('#contemcores').append(retornojson.mensagem);
                $('#add-cor-tit').val('');
                $('#add-cor-hex').val('');
            }
        });
    });

    var clone_labels = $('.labels-originais').html() + '<br>';

    bindSelectCidades();

    $('.seleciona_estado').each( function(){
        if($(this).val() != ''){
            var alvo = $(this).parent().next().find('.seleciona_cidade');
            filtraCidades($(this).val(), alvo);
        }
    });

    $('#acrescentar-cidade').click( function(e){
        e.preventDefault();
        $('.cidades-forms').append(clone_labels);
        bindSelectCidades();
    });

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

    if($('textarea.cke').length){
        $('textarea.cke.fundoAzul').ckeditor({
            extraPlugins : 'font,colorbutton',
            contentsCss : '/assets/vendor/ckeditor/contentsFundoAzul.css'            
        });
        $('textarea.cke.fundoBranco').ckeditor({
            extraPlugins : 'font,colorbutton,injectimage',
            contentsCss : '/assets/vendor/ckeditor/contentsFundoBranco.css',
            extraAllowedContent: 'img[*]',
            ignoreEmptyParagraph : false,            
        });
        $('textarea.cke.fundoBrancoSemImagem').ckeditor({
            extraPlugins : 'font,colorbutton',
            contentsCss : '/assets/vendor/ckeditor/contentsFundoBranco.css',            
        });
    }
    	
});	
