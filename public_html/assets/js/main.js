$('document').ready( function(){

	$('.index-categorias .categoria').on({ 'click' : function(e){
		$(this).find('.produtos ul').toggleClass('aberto');
	} });

	$('#scroll-topo').click( function(e){
		e.preventDefault();
		$('body, html').animate({'scrollTop' : 0}, 100);
	});

	$('#select-estado').simpleselect();

	$('#representante-form').submit( function(){
		if(!$('#select-estado').val()){
			alert('Selecione um estado!')
			e.preventDefault();
			return false;
		}
	});

	if($('.main-noticias #lateral ul').length > 1){
		var atual = $('.main-noticias #lateral ul li a.ativo');
		var parent_atual = atual.parent().parent();
		var startingSlide = parent_atual.index();
		if(startingSlide != 0){
			$('.main-noticias #lateral').cycle({
				timeout : 0,
				next : $('#btn-ver-mais'),
				startingSlide : startingSlide
			});
		}else{
			$('.main-noticias #lateral').cycle({
				timeout : 0,
				next : $('#btn-ver-mais')
			});
		}
	}else{
		$('#btn-ver-mais').hide();
	}

	$('#lista-area-texto a').click( function(e){
		e.preventDefault();
		$(this).find('.descricao').fadeIn('slow');
	});

	var slideshow = setInterval( function(){
		$('#banners #thumbs a.thumb:not(.aberto):last').click();
	}, 6000);

	var slideshow_tablet = setInterval( function(){
		$('#banners-tablet #thumbs a.thumb:not(.aberto):last').click();
	}, 6000);

	$('#banners #thumbs a').click( function(e){
		e.preventDefault();
		var id = $(this).attr('data-target');
		var self = $(this);
		if(!$('#banners').hasClass('animando')){
			clearInterval(slideshow);
			$('#banners').addClass('animando');
			$(this).addClass('aberto');
			setTimeout( function(){
				self.addClass('suma');
				$('#banners #thumbs a.aberto').not(self).removeClass('aberto suma').prependTo($('#banners #thumbs'));
				$('#banners #ampliadas .aberto').removeClass('aberto');
				$("#banners #ampliadas *[data-id='"+id+"']").addClass('aberto');
				$('#banners').removeClass('animando');
				slideshow = setInterval( function(){
					$('#banners #thumbs a.thumb:not(.aberto):last').click();
				}, 6000);
			}, 600);
		}
	});

	$('#banners-tablet #thumbs a').click( function(e){
		e.preventDefault();
		var id = $(this).attr('data-target');
		var self = $(this);
		if(!$('#banners-tablet').hasClass('animando')){
			clearInterval(slideshow);
			$('#banners-tablet').addClass('animando');
			$(this).addClass('aberto');
			setTimeout( function(){
				self.addClass('suma');
				$('#banners-tablet #thumbs a.aberto').not(self).removeClass('aberto suma').prependTo($('#banners-tablet #thumbs'));
				$('#banners-tablet #ampliadas .aberto').removeClass('aberto');
				$("#banners-tablet #ampliadas *[data-id='"+id+"']").addClass('aberto');
				$('#banners-tablet').removeClass('animando');
				slideshow = setInterval( function(){
					$('#banners-tablet #thumbs a.thumb:not(.aberto):last').click();
				}, 6000);
			}, 600);
		}
	});	

	$('#file-trabalho').change( function(e){
		var arquivo = e.target.files[0].name;
		$('.fake-file span').html(arquivo);
	});

	$('.lista-trabalhos a, .lista-trabalhos-mobile a').fancybox({
		padding : 0,
		margin: 60,
		titlePosition : 'outside',
		beforeShow : function(){
			this.title = "<h1>"+$(this.element).attr('data-hum')+"</h1><h2>"+$(this.element).attr('data-hdois')+"</h2>"
		},
		helpers : {
            title : {
                type: 'inside'
            }
        }
	});

	if($('.shadowProduto').length){
		$.fancybox.helpers.thumbs.onUpdate = function() {};
		$('.shadowProduto').fancybox({
			padding : 0,
			margin: 60,
			title: null,
			helpers : {
	            thumbs	: {
					width	: 60,
					height	: 60
				}
			}
		});
	}

});
