@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        {{$texto->titulo}} - Conteúdo <a href='{{ URL::route('painel.graficotextositens.create', array('texto_id' => $texto->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a>
    </h2>
    <hr>
    <a href="{{URL::route('painel.graficotextos.index')}}" title="Voltar" class="btn btn-default">&larr; Voltar para Área do Gráfico - Textos</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Descrição</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->descricao), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.graficotextositens.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.graficotextositens.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
</div>

@stop