<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Bignardi Papéis - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Bignardi Papéis</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.banners.index')}}" title="Banners">Banners</a>
						</li>

						<li @if(str_is('painel.quemsomos*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.quemsomos.index')}}" title="Quem Somos">Quem Somos</a>
						</li>

						<li @if(str_is('painel.certificacoes.*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.certificacoes.index')}}" title="Certificações">Certificações</a>
						</li>

						<li @if(str_is('painel.representantes*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.representantes.index')}}" title="Representantes">Representantes</a>
						</li>

						<li @if(str_is('painel.noticias*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.noticias.index')}}" title="Notícias">Notícias</a>
						</li>

						<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li class="dropdown @if(str_is('painel.produtos*', Route::currentRouteName()) || str_is('painel.categorias*', Route::currentRouteName()) || str_is('painel.cores*', Route::currentRouteName()) || str_is('painel.produtoscertificacoes*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.produtos.index')}}" title="Produtos">Produtos</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{URL::route('painel.categorias.index')}}" title="Categorias">Categorias</a></li>
								<li><a href="{{URL::route('painel.cores.index')}}" title="Cores">Cores</a></li>
								<li><a href="{{URL::route('painel.produtoscertificacoes.index')}}" title="Certificações">Certificações</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.sustentabilidade*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.sustentabilidade.index')}}" title="Sustentabilidade">Sustentabilidade</a>
						</li>

						<li class="dropdown @if(str_is('painel.grafico*', Route::currentRouteName()) || str_is('painel.grafico*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Área do Gráfico <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.graficoarquivos.index')}}" title="Arquivos">Arquivos</a></li>
								<li><a href="{{URL::route('painel.graficotextos.index')}}" title="Textos">Textos</a></li>								
							</ul>
						</li>

						<li @if(str_is('painel.trabalhos*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.trabalhos.index')}}" title="Trabalhos">Trabalhos</a>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
