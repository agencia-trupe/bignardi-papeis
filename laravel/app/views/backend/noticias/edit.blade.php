@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Notícia
        </h2>  

		{{ Form::open( array('route' => array('painel.noticias.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" value="{{ Tools::converteData($registro->data) }}" required>
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem de Destaque Atual<br>
						<img src="assets/images/noticias/{{$registro->imagem}}"><br>
						<label><input type="checkbox" name="remover_imagem" value="1"> remover imagem</label><br><br>
					@endif
					<label for="inputImagem de Destaque">Trocar Imagem de Destaque</label>
					<input type="file" class="form-control" id="inputImagem de Destaque" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" required>{{$registro->olho }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke fundoBrancoSemImagem" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.noticias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop