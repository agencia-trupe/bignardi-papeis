@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Notícia
        </h2>  

		<form action="{{URL::route('painel.noticias.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputImagem de Destaque">Imagem de Destaque</label>
					<input type="file" class="form-control" id="inputImagem de Destaque" name="imagem" >
				</div>

				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" required>@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho'] }} @endif</textarea>
				</div>	

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke fundoBrancoSemImagem" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>	
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.noticias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop