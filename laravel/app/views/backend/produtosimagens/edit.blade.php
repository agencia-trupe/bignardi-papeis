@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Imagem do produto <strong>{{$produto->titulo}}</strong> 
        </h2>

		{{ Form::open( array('route' => array('painel.produtosimagens.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/produtosimagens/{{$registro->imagem}}" style="max-width:700px;"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<input type="hidden" name="id_produtos" value="{{$produto->id}}">

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.produtosimagens.index', array('id_produtos' => $produto->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop