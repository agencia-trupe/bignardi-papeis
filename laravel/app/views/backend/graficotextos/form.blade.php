@section('conteudo')

    <div class="container add">

      	<h2>
        	Área do Gráfico - Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.graficotextos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputDescrição">Descrição</label>
					<textarea name="descricao" class="form-control" id="inputDescrição" required>@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['descricao'] }} @endif</textarea>
				</div>	

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.graficotextos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop