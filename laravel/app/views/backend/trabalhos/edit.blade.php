@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Trabalho
        </h2>  

		{{ Form::open( array('route' => array('painel.trabalhos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" name="email" value="{{$registro->email}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" >
				</div>
				
				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" >
				</div>
				
				<div class="form-group">
					<label for="inputLegenda">Legenda</label>
					<input type="text" class="form-control" id="inputLegenda" name="legenda" value="{{$registro->legenda}}" >
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/trabalhos/{{$registro->imagem}}" style="max-width:700px;"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<hr>
				<label>
					<input type="checkbox" name="aprovado" value="1" @if($registro->aprovado == '1') checked @endif> APROVADO
				</label>
				<hr>
				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.trabalhos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop