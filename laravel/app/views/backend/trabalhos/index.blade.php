@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Trabalhos <a href='{{ URL::route('painel.trabalhos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Trabalho</a>
    </h2>

    <hr>
    <div class="btn-group">
        <a href="{{URL::route('painel.trabalhos.index', array('aprovado' => 0))}}" title="Trabalhos Aguardando Aprovação" class="btn btn-sm btn-default @if($aprovados == 0) btn-info @endif">Trabalhos Aguardando Aprovação</a>
        <a href="{{URL::route('painel.trabalhos.index', array('aprovado' => 1))}}" title="Trabalhos Aprovados" class="btn btn-sm btn-default @if($aprovados == 1) btn-info @endif">Trabalhos Aprovados</a>
    </div>
    <hr>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Nome</th>
				<th>Legenda</th>
				<th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
				<td>{{ $registro->legenda }}</td>
				<td><img src='assets/images/trabalhos/{{ $registro->imagem }}' style='max-width:150px'></td>
                    @if($aprovados == 1)
                        <td class="crud-actions" style="width:180px;">
                            <a href='{{ URL::route('painel.trabalhos.edit', $registro->id ) }}' title="visualizar" class='btn btn-primary btn-sm'>visualizar</a>
                    @else
                        <td class="crud-actions" style="width:205px;">
                            <a href='{{ URL::route('painel.trabalhos.edit', $registro->id ) }}' title="visualizar e aprovar" class='btn btn-primary btn-sm'>visualizar e aprovar</a>
                    @endif

                    {{ Form::open(array('route' => array('painel.trabalhos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop