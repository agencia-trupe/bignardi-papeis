@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Cor
        </h2>

		<form action="{{URL::route('painel.cores.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputTítuloEn">Título (inglês)</label>
					<input type="text" class="form-control" id="inputTítuloEn" name="titulo_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTítuloEs">Título (espanhol)</label>
					<input type="text" class="form-control" id="inputTítuloEs" name="titulo_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_es'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputHexadecimal">Hexadecimal</label>
					<input maxlength="7" type="text" class="form-control" id="inputHexadecimal" name="hex" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['hex'] }}" @endif required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.cores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop