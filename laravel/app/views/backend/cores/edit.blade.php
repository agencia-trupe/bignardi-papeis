@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Cor
        </h2>

		{{ Form::open( array('route' => array('painel.cores.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				<div class="form-group">
					<label for="inputTítuloEn">Título (inglês)</label>
					<input type="text" class="form-control" id="inputTítuloEn" name="titulo_en" value="{{$registro->titulo_en}}">
				</div>
				<div class="form-group">
					<label for="inputTítuloEs">Título (espanhol)</label>
					<input type="text" class="form-control" id="inputTítuloEs" name="titulo_es" value="{{$registro->titulo_es}}">
				</div>
				<div class="form-group">
					<label for="inputHexadecimal">Hexadecimal</label>
					<input maxlength="7" type="text" class="form-control" id="inputHexadecimal" name="hex" value="{{$registro->hex}}" required>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.cores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop