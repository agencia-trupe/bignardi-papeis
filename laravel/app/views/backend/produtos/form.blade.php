@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Produto
        </h2>

		<form action="{{URL::route('painel.produtos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select	name="categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if(sizeof($categorias) > 0)
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputTítuloEn">Título (inglês)</label>
					<input type="text" class="form-control" id="inputTítuloEn" name="titulo_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_en'] }}" @endif required>
				</div>
				<div class="form-group">
					<label for="inputTítuloEs">Título (espanhol)</label>
					<input type="text" class="form-control" id="inputTítuloEs" name="titulo_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_es'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>

				<div class="form-group">
					<label for="inputTipo">Tipo</label>
					<input type="text" class="form-control" id="inputTipo" name="tipo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTipoEn">Tipo (inglês)</label>
					<input type="text" class="form-control" id="inputTipoEn" name="tipo_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTipoEs">Tipo (espanhol)</label>
					<input type="text" class="form-control" id="inputTipoEs" name="tipo_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo_es'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputDescrição">Descrição</label>
					<textarea name="descricao" class="form-control" id="inputDescrição" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['descricao'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputDescriçãoEn">Descrição (inglês)</label>
					<textarea name="descricao_en" class="form-control" id="inputDescriçãoEn" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['descricao_en'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputDescriçãoEs">Descrição (espanhol)</label>
					<textarea name="descricao_es" class="form-control" id="inputDescriçãoEs" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['descricao_es'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputTipo de Impressão">Tipo de Impressão</label>
					<input type="text" class="form-control" id="inputTipo de Impressão" name="tipo_impressao" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo_impressao'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTipo de ImpressãoEn">Tipo de Impressão (inglês)</label>
					<input type="text" class="form-control" id="inputTipo de ImpressãoEn" name="tipo_impressao_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo_impressao_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTipo de ImpressãoEs">Tipo de Impressão (espanhol)</label>
					<input type="text" class="form-control" id="inputTipo de ImpressãoEs" name="tipo_impressao_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo_impressao_es'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputAplicações">Aplicações</label>
					<textarea name="aplicacoes" class="form-control" id="inputAplicações" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['aplicacoes'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputAplicaçõesEn">Aplicações (inglês)</label>
					<textarea name="aplicacoes_en" class="form-control" id="inputAplicaçõesEn" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['aplicacoes_en'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputAplicaçõesEs">Aplicações (espanhol)</label>
					<textarea name="aplicacoes_es" class="form-control" id="inputAplicaçõesEs" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['aplicacoes_es'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputPH">PH</label>
					<input type="text" class="form-control" id="inputPH" name="ph" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['ph'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputPHEn">PH (inglês)</label>
					<input type="text" class="form-control" id="inputPHEn" name="ph_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['ph_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputPHEs">PH (espanhol)</label>
					<input type="text" class="form-control" id="inputPHEs" name="ph_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['ph_es'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputGramatura">Gramatura</label>
					<input type="text" class="form-control" id="inputGramatura" name="gramatura" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['gramatura'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputGramaturaEn">Gramatura (inglês)</label>
					<input type="text" class="form-control" id="inputGramaturaEn" name="gramatura_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['gramatura_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputGramaturaEs">Gramatura (espanhol)</label>
					<input type="text" class="form-control" id="inputGramaturaEs" name="gramatura_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['gramatura_es'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputFolhas">Folhas</label>
					<input type="text" class="form-control" id="inputFolhas" name="folhas" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['folhas'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputFolhasEn">Folhas (inglês)</label>
					<input type="text" class="form-control" id="inputFolhasEn" name="folhas_en" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['folhas_en'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputFolhasEs">Folhas (espanhol)</label>
					<input type="text" class="form-control" id="inputFolhasEs" name="folhas_es" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['folhas_es'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputBobinas">Bobinas</label>
					<textarea name="bobinas" class="form-control" id="inputBobinas" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['bobinas'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputBobinasEn">Bobinas (inglês)</label>
					<textarea name="bobinas_en" class="form-control" id="inputBobinasEn" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['bobinas_en'] }} @endif</textarea>
				</div>
				<div class="form-group">
					<label for="inputBobinasEs">Bobinas (espanhol)</label>
					<textarea name="bobinas_es" class="form-control" id="inputBobinasEs" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['bobinas_es'] }} @endif</textarea>
				</div>

				<hr>
				<div class="form-group">
					<label for="inputCertificacoes">Certificações</label><br>
					@if($certificacoes)
						@foreach($certificacoes as $certificacoes)
							<label style="width: 30%;text-align:center;">
								<img src="assets/images/produtoscertificacoes/{{$certificacoes->imagem}}"><br>
								<input type="checkbox" name="certificacoes[]" value="{{$certificacoes->id}}">
								{{$certificacoes->titulo}}
							</label>
						@endforeach
					@endif
				</div>

				<hr>

				<div class="form-group">
					<label for="inputCores">Cores</label><br>
					<div id="contemcores">
						@if($cores)
							@foreach($cores as $cor)
								<label style="width: 30%;"><input type="checkbox"  name="cores[]" value="{{$cor->id}}"> <span style="margin:0; width:10px; height:10px; display:inline-block; border-radius:100%; background:{{$cor->hex}}; border:1px #ccc solid;"></span> {{$cor->titulo}}</label>
							@endforeach
						@endif
					</div>
					<hr>
					<div>
						<a href="#" id="add-cor" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#formcores"><span class="glyphicon glyphicon-plus-sign"></span> Nova cor</a>
						<div style="margin-top:10px;" id="formcores" class="collapse form-inline">
							<input type="text" name="titulo_cor" class="form-control" placeholder="Nome da Cor" id="add-cor-tit">
							<input type="text" name="hex" class="form-control" maxlength="7" placeholder="Código Hexadecimal" id="add-cor-hex">
							<input type="submit" value="Adicionar" class="btn btn-success btn-sm" id="submit-cor">
						</div>
					</div>
				</div>

				<hr>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir Produto</button>

				<a href="{{URL::route('painel.produtos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop