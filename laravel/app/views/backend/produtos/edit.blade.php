@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Produto
        </h2>

		{{ Form::open( array('route' => array('painel.produtos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select	name="categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if(sizeof($categorias) > 0)
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}" @if($categoria->id == $registro->categorias_id) selected @endif>{{$categoria->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				<div class="form-group">
					<label for="inputTítuloEn">Título (inglês)</label>
					<input type="text" class="form-control" id="inputTítuloEn" name="titulo_en" value="{{$registro->titulo_en}}">
				</div>
				<div class="form-group">
					<label for="inputTítuloEs">Título (espanhol)</label>
					<input type="text" class="form-control" id="inputTítuloEs" name="titulo_es" value="{{$registro->titulo_es}}">
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/produtos/thumbs/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputTipo">Tipo</label>
					<input type="text" class="form-control" id="inputTipo" name="tipo" value="{{$registro->tipo}}" >
				</div>
				<div class="form-group">
					<label for="inputTipoEn">Tipo (inglês)</label>
					<input type="text" class="form-control" id="inputTipoEn" name="tipo_en" value="{{$registro->tipo_en}}">
				</div>
				<div class="form-group">
					<label for="inputTipoEs">Tipo (espanhol)</label>
					<input type="text" class="form-control" id="inputTipoEs" name="tipo_es" value="{{$registro->tipo_es}}">
				</div>

				<div class="form-group">
					<label for="inputDescrição">Descrição</label>
					<textarea name="descricao" class="form-control" id="inputDescrição" >{{$registro->descricao }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputDescricaoEn">Descrição (inglês)</label>
					<textarea name="descricao_en" class="form-control" id="inputDescricaoEn" >{{$registro->descricao_en }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputDescricaoEs">Descrição (espanhol)</label>
					<textarea name="descricao_es" class="form-control" id="inputDescricaoEs" >{{$registro->descricao_es }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTipo de Impressão">Tipo de Impressão</label>
					<input type="text" class="form-control" id="inputTipo de Impressão" name="tipo_impressao" value="{{$registro->tipo_impressao}}" >
				</div>
				<div class="form-group">
					<label for="inputTipoImpressaoEn">Tipo de Impressão (inglês)</label>
					<input type="text" class="form-control" id="inputTipoImpressaoEn" name="tipo_impressao_en" value="{{$registro->tipo_impressao_en}}" >
				</div>
				<div class="form-group">
					<label for="inputTipoImpressaoEs">Tipo de Impressão (espanhol)</label>
					<input type="text" class="form-control" id="inputTipoImpressaoEs" name="tipo_impressao_es" value="{{$registro->tipo_impressao_es}}" >
				</div>

				<div class="form-group">
					<label for="inputAplicações">Aplicações</label>
					<textarea name="aplicacoes" class="form-control" id="inputAplicações" >{{$registro->aplicacoes }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputAplicaçõesEn">Aplicações (inglês)</label>
					<textarea name="aplicacoes_en" class="form-control" id="inputAplicaçõesEn" >{{$registro->aplicacoes_en }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputAplicaçõesEs">Aplicações (espanhol)</label>
					<textarea name="aplicacoes_es" class="form-control" id="inputAplicaçõesEs" >{{$registro->aplicacoes_es }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputPH">PH</label>
					<input type="text" class="form-control" id="inputPH" name="ph" value="{{$registro->ph}}" >
				</div>
				<div class="form-group">
					<label for="inputPHEn">PH (inglês)</label>
					<input type="text" class="form-control" id="inputPHEn" name="ph_en" value="{{$registro->ph_en}}" >
				</div>
				<div class="form-group">
					<label for="inputPHEs">PH (espanhol)</label>
					<input type="text" class="form-control" id="inputPHEs" name="ph_es" value="{{$registro->ph_es}}" >
				</div>

				<div class="form-group">
					<label for="inputGramatura">Gramatura</label>
					<input type="text" class="form-control" id="inputGramatura" name="gramatura" value="{{$registro->gramatura}}" >
				</div>
				<div class="form-group">
					<label for="inputGramaturaEn">Gramatura (inglês)</label>
					<input type="text" class="form-control" id="inputGramaturaEn" name="gramatura_en" value="{{$registro->gramatura_en}}" >
				</div>
				<div class="form-group">
					<label for="inputGramaturaEs">Gramatura (espanhol)</label>
					<input type="text" class="form-control" id="inputGramaturaEs" name="gramatura_es" value="{{$registro->gramatura_es}}" >
				</div>

				<div class="form-group">
					<label for="inputFolhas">Folhas</label>
					<input type="text" class="form-control" id="inputFolhas" name="folhas" value="{{$registro->folhas}}" >
				</div>
				<div class="form-group">
					<label for="inputFolhasEn">Folhas (inglês)</label>
					<input type="text" class="form-control" id="inputFolhasEn" name="folhas_en" value="{{$registro->folhas_en}}" >
				</div>
				<div class="form-group">
					<label for="inputFolhasEs">Folhas (espanhol)</label>
					<input type="text" class="form-control" id="inputFolhasEs" name="folhas_es" value="{{$registro->folhas_es}}" >
				</div>

				<div class="form-group">
					<label for="inputBobinas">Bobinas</label>
					<textarea name="bobinas" class="form-control" id="inputBobinas" >{{$registro->bobinas }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputBobinasEn">Bobinas (inglês)</label>
					<textarea name="bobinas_en" class="form-control" id="inputBobinasEn" >{{$registro->bobinas_en }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputBobinasEs">Bobinas (espanhol)</label>
					<textarea name="bobinas_es" class="form-control" id="inputBobinasEs" >{{$registro->bobinas_es }}</textarea>
				</div>

				<hr>
				<div class="form-group">
					<label for="inputCertificacoes">Certificações</label><br>
					@if($certificacoes)
						@foreach($certificacoes as $certificacao)
							<label style="width: 30%;text-align:center;">
								<img src="assets/images/produtoscertificacoes/{{$certificacao->imagem}}"><br>
								<input type="checkbox" name="certificacoes[]" value="{{$certificacao->id}}" @if(in_array($certificacao->id, $registro->certificacoesArray())) checked @endif>
								{{$certificacao->titulo}}
							</label>
						@endforeach
					@endif
				</div>

				<hr>

				<div class="form-group">
					<label for="inputCores">Cores</label><br>
					<div id="contemcores">
						@if($cores)
							@foreach($cores as $cor)
								<label style="width: 30%;">
									<input type="checkbox" name="cores[]" value="{{$cor->id}}" @if(in_array($cor->id, $registro->coresArray())) checked @endif> <span style="margin:0; width:10px; height:10px; display:inline-block; border-radius:100%; background:{{$cor->hex}}; border:1px #ccc solid;"></span> {{$cor->titulo}}
								</label>
							@endforeach
						@endif
					</div>
					<hr>
					<div>
						<a href="#" id="add-cor" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#formcores"><span class="glyphicon glyphicon-plus-sign"></span> Nova cor</a>
						<div style="margin-top:10px;" id="formcores" class="collapse form-inline">
							<input type="text" name="titulo_cor" class="form-control" placeholder="Nome da Cor" id="add-cor-tit">
							<input type="text" name="hex" class="form-control" maxlength="7" placeholder="Código Hexadecimal" id="add-cor-hex">
							<input type="submit" value="Adicionar" class="btn btn-success btn-sm" id="submit-cor">
						</div>
					</div>
				</div>

				<hr>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.produtos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop