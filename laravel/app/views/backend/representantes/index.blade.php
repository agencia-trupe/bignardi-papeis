@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Representantes <a href='{{ URL::route('painel.representantes.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Representante</a>
    </h2>

    <hr>
        {{ Form::open( array('route' => 'painel.representantes.index', 'method' => 'get', 'class' => 'form-inline') ) }}
            <label>
                <select name="filtra-estado" class="form-control seleciona_estado">
                    <option value="">Todos os Estados</option>
                    @foreach ($estados as $estado)
                        <option value="{{ $estado->id }}" @if($estado->id == $filtra_estado) selected @endif>{{ $estado->nome }}</option>
                    @endforeach;
                </select>
            </label>

            <button type="submit" class="btn btn-info">Filtrar Resultados</button>
        {{ Form::close() }}
    <hr>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Área de atuação</th>
                <th>Empresa</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->regiao }}</td>
                <td>{{ $registro->empresa }}</td>
                <td>{{ Str::words($registro->empresa, 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.representantes.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.representantes.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop