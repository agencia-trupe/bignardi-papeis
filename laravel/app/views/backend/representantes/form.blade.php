@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Representante
        </h2>  

		<form action="{{URL::route('painel.representantes.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputRegião">Região</label>
					<input type="text" class="form-control" id="inputRegião" name="regiao" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario');?>{{$s['regiao']}}" @endif >
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario');?>{{$s['empresa']}}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke fundoBrancoSemImagem" id="inputTexto" >@if(Session::has('formulario')) <?php $s=Session::get('formulario');?>{{$s['texto']}} @endif</textarea>
				</div>	

				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario');?>{{$s['email']}}" @endif >
				</div>
				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario');?>{{$s['cep']}}" @endif >
				</div>

				<hr>

				Área de Atuação<br><br>

				<div class="form-inline cidades-forms" style="white-space:nowrap;">

					<span class="labels-originais">

						<label>Estado<br>
						<select name="representantes_estado_id[]" class="form-control seleciona_estado" required required-message="O estado é obrigatório!">
							<option value="">Selecione um Estado</option>
							@foreach ($estados as $estado)
								<option value="{{ $estado->id }}">{{ $estado->uf.' - '.$estado->nome }}</option>
							@endforeach
						</select></label>
						
					</span>

					<a href="#" class="btn btn-xs btn-primary" id="acrescentar-cidade"><i class="icon-plus-sign icon-white"></i> Acrescentar Estado</a> <br>

				</div>

				<br><br>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.representantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop