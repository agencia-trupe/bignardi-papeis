@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Representante
        </h2>  

		{{ Form::open( array('route' => array('painel.representantes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputRegião">Região</label>
					<input type="text" class="form-control" id="inputRegião" name="regiao" value="{{$registro->regiao}}" >
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" required>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke fundoBrancoSemImagem" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" >
				</div>

				<div class="form-group">
					<label for="inputCEP">CEP</label>
					<input type="text" class="form-control" id="inputCEP" name="cep" value="{{$registro->cep}}" >
				</div>

				Área de Atuação<br><br>

				<div class="form-inline cidades-forms" style="white-space:nowrap;">

					@if(sizeof($locais))
						@foreach($locais as $key => $local)
							
							<label>Estado<br>
							<select name="representantes_estado_id[]" class="form-control seleciona_estado" required required-message="O estado é obrigatório!">
								<option value="">Selecione um Estado</option>
								@foreach ($estados as $estado)
									<option value="{{ $estado->id }}" @if($local->estado_id==$estado->id) selected @endif>{{ $estado->uf.' - '.$estado->nome }}</option>
								@endforeach
							</select></label>

							<label>
								<a href="#" class="btn btn-xs btn-danger btn-remover" title="remover"><i class="icon-remove-sign icon-white"></i> Remover</a>
							</label>

							@if($key == 0)
								<a href="#" class="btn btn-xs btn-primary" id="acrescentar-cidade"><i class="icon-plus-sign icon-white"></i> Acrescentar Estado/Cidade</a>
							@endif

							<br>
						@endforeach
					@else
						<a href="#" class="btn btn-xs btn-primary" id="acrescentar-cidade"><i class="icon-plus-sign icon-white"></i> Acrescentar Estado/Cidade</a><br>
					@endif

					<span class="labels-originais" style="display:none;">

						<label>Estado<br>
						<select name="representantes_estado_id[]" class="form-control seleciona_estado" required-message="O estado é obrigatório!">
							<option value="">Selecione um Estado</option>
							@foreach ($estados as $estado)
								<option value="{{ $estado->id }}">{{ $estado->uf.' - '.$estado->nome }}</option>
							@endforeach
						</select></label>

						<label>
							<a href="#" class="btn btn-xs btn-danger btn-remover" title="remover"><i class="icon-remove-sign icon-white"></i> Remover</a>
						</label>

					</span>

				</div><br><br>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.representantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop