@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações de Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" >
				</div>
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" >
				</div>
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$registro->facebook}}" >
				</div>
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" value="{{$registro->twitter}}" >
				</div>
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >{{$registro->endereco }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputEndereço (exibido no rodapé do site)">Endereço (exibido no rodapé do site)</label>
					<textarea name="endereco_rodape" class="form-control" id="inputEndereço (exibido no rodapé do site)" >{{$registro->endereco_rodape }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputGoogle Maps (código de incorporação)">Google Maps (código de incorporação)</label>
					<input type="text" class="form-control" id="inputGoogle Maps (código de incorporação)" name="google_maps" value='{{$registro->google_maps}}' >
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop