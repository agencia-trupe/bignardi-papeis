@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.quemsomos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputHistória">História</label>
					<textarea name="historia" class="form-control" id="inputHistória" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['historia'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputMissão">Missão</label>
					<textarea name="missao" class="form-control cke fundoAzul" id="inputMissão" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['missao'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputVisão">Visão</label>
					<textarea name="visao" class="form-control cke fundoAzul" id="inputVisão" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['visao'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputValores">Valores</label>
					<textarea name="valores" class="form-control cke fundoAzul" id="inputValores" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['valores'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoImagem">Texto sobre a Imagem</label>
					<textarea name="texto_imagem" class="form-control cke fundoAzul" id="inputTextoImagem" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_imagem'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.quemsomos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop