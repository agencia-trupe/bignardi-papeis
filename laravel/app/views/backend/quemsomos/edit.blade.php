@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.quemsomos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputHistória">História</label>
					<textarea name="historia" class="form-control cke fundoBranco" id="inputHistória" >{{$registro->historia }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputMissão">Missão</label>
					<textarea name="missao" class="form-control cke fundoAzul" id="inputMissão" >{{$registro->missao }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputVisão">Visão</label>
					<textarea name="visao" class="form-control cke fundoAzul" id="inputVisão" >{{$registro->visao }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputValores">Valores</label>
					<textarea name="valores" class="form-control cke fundoAzul" id="inputValores" >{{$registro->valores }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoImagem">Texto sobre a Imagem</label>
					<textarea name="texto_imagem" class="form-control cke fundoAzul" id="inputTextoImagem" >{{$registro->texto_imagem }}</textarea>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/quemsomos/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.quemsomos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop