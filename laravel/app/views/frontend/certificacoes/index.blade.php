@section('conteudo')

	<h1 class="page-title">Certificações</h1>

	@if(sizeof($certificacoes))
		<div class="pure-g-r listaCertificacoes">
			@foreach($certificacoes as $certificacao)
				<div class="pure-u-1-4 certificacao">
					<div class="pad">
						<div class="imagem">
							@if($certificacao->imagem)
								<img src="assets/images/certificacoes/{{$certificacao->imagem}}" alt="{{$certificacao->titulo}}">
							@endif
						</div>
						<div class="cke">
							{{$certificacao->texto}}
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif

@stop
