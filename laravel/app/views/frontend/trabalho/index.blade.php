@section('conteudo')
	<h1 class="page-title">Galeria de Trabalhos</h1>

	<div class="pure-g-r">
		<div class="pure-u-7-24 intro">
			<p>
				Aqui, designers e profissionais gráficos podem mostrar seus trabalhos.
			</p>
			<p>
				Envie já o seu!
			</p>

			<form action="" method="post" enctype="multipart/form-data" novalidate>
				@if(Session::has('enviado'))
					<div class="success">Sua mensagem foi enviada com sucesso!</div>
				@endif
				@if(Session::has('erro'))
					<div class="error">{{Session::get('erro')}}</div>
				@endif
				<input type="text" name="nome" placeholder="nome" required value="{{Session::get('envio-nome')}}">
				<input type="email" name="email" placeholder="e-mail" required value="{{Session::get('envio-email')}}">
				<input type="text" name="telefone" placeholder="telefone" value="{{Session::get('envio-telefone')}}">
				<input type="text" name="empresa" placeholder="empresa" value="{{Session::get('envio-empresa')}}">
				<input type="text" name="legenda" placeholder="legenda do trabalho" required value="{{Session::get('envio-legenda')}}">
				<label>
					<div class="fake-file">
						<span>anexar arquivo</span>
					</div>
					<input type="file" name="arquivo" id="file-trabalho" required>
				</label>
				<input type="submit" value="ENVIAR">
			</form>

			<small>
				Os trabalhos recebidos serão analisados pela equipe da Bignardi Papéis e publicados em nossa Galeria de Trabalhos após triagem.
			</small>
		</div>
		<div class="pure-u-17-24">

			<div class="hide-mobile">

				<div class="lista-trabalhos">
					@if(sizeof($trabalhos))
						@foreach($trabalhos as $trab)
							<a href="assets/images/trabalhos/{{$trab->imagem}}" title="{{$trab->nome}}" rel="trabalhos" data-hum="{{$trab->nome}}" data-hdois="{{$trab->legenda}}">
								<img src="assets/images/trabalhos/thumbs/{{$trab->imagem}}" alt="{{$trab->nome}}">
								<div class="overlay">
									<div class="nome">{{$trab->nome}}</div>
									<div class="titulo-trabalho">{{$trab->legenda}}</div>
								</div>
							</a>
						@endforeach
					@endif
				</div>

				{{$trabalhos->links()}}

			</div>

			<div class="hide-desktop hide-tablet">
				<div class="pure-g lista-trabalhos-mobile">
					@if(sizeof($trabalhos))
						@foreach($trabalhos as $trab)
							<div class="pure-u-1-2">
								<a href="assets/images/trabalhos/{{$trab->imagem}}" title="{{$trab->nome}}" rel="trabalhos_mobile" data-hum="{{$trab->nome}}" data-hdois="{{$trab->legenda}}">
									<img src="assets/images/trabalhos/thumbs/{{$trab->imagem}}" alt="{{$trab->nome}}">
								</a>
							</div>
						@endforeach
					@endif
				</div>				
			</div>


		</div>
	</div>	
@stop
