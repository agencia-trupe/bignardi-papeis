@section('conteudo')
	<h1 class="page-title">Sustentabilidade</h1>
	
	@if(sizeof($texto))
		<div class="detalhes pure-g-r">
			<div class="pure-u-1-2">
				<div class="imagem">
					<div class="in-pad">
						<img src="assets/images/sustentabilidade/{{$texto->imagem}}" alt="{{$texto->titulo}}">
						<h2>{{$texto->titulo}}</h2>
					</div>
				</div>
			</div>
			<div class="pure-u-1-2">
				<div class="texto cke">
					{{$texto->texto}}
				</div>
			</div>
		</div>
	@else
		<h2>Texto não encontrado!</h2>
	@endif

	<div class="saibaMais hide-mobile">
		<h3>Saiba mais sobre SUSTENTABILIDADE &raquo;</h3>
		@if(sizeof($outrosTextos))
			<div class="pure-g-r">
				@foreach($outrosTextos as $k => $texto)
					<div class="pure-u-1-4">
						<div class="in-pad">
							<a href="sustentabilidade/{{$texto->slug}}" title="{{$texto->titulo}}">
								<img src="assets/images/sustentabilidade/thumbs/{{$texto->imagem}}" alt="{{$texto->titulo}}">
								<h2>{{$texto->titulo}}</h2>
							</a>
						</div>
					</div>
				@endforeach
			</div>
		@endif
	</div>

	<a href="sustentabilidade" title="Saiba mais sobre SUSTENTABILIDADE" class="btn-voltar-sustentabilidade hide-desktop hide-tablet">&laquo; Saiba mais sobre SUSTENTABILIDADE</a>
@stop