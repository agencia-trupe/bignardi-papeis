@section('conteudo')
	<h1 class="page-title">Sustentabilidade</h1>

	<div class="pure-g-r sustentabilidade-index">
		<div class="pure-u-1-2">
			@if(isset($sustentabilidade[0]))
				<a href="sustentabilidade/{{$sustentabilidade[0]->slug}}" title="{{$sustentabilidade[0]->titulo}}">
					<img src="assets/images/sustentabilidade/{{$sustentabilidade[0]->imagem}}" alt="{{$sustentabilidade[0]->titulo}}">
					<h2 class="large">{{$sustentabilidade[0]->titulo}}</h2>
				</a>
			@endif
		</div>
		<div class="pure-u-1-2">
			<div class="pure-g">
				<div class="pure-u-1-2">
					@if(isset($sustentabilidade[1]))
						<a href="sustentabilidade/{{$sustentabilidade[1]->slug}}" title="{{$sustentabilidade[1]->titulo}}">
							<img src="assets/images/sustentabilidade/thumbs/{{$sustentabilidade[1]->imagem}}" alt="{{$sustentabilidade[1]->titulo}}">
							<h2>{{$sustentabilidade[1]->titulo}}</h2>
						</a>
					@endif
				</div>
				<div class="pure-u-1-2">
					@if(isset($sustentabilidade[2]))
						<a href="sustentabilidade/{{$sustentabilidade[2]->slug}}" title="{{$sustentabilidade[2]->titulo}}">
							<img src="assets/images/sustentabilidade/thumbs/{{$sustentabilidade[2]->imagem}}" alt="{{$sustentabilidade[2]->titulo}}">
							<h2>{{$sustentabilidade[2]->titulo}}</h2>
						</a>
					@endif
				</div>
				<div class="pure-u-1-2">
					@if(isset($sustentabilidade[3]))
						<a href="sustentabilidade/{{$sustentabilidade[3]->slug}}" title="{{$sustentabilidade[3]->titulo}}">
							<img src="assets/images/sustentabilidade/thumbs/{{$sustentabilidade[3]->imagem}}" alt="{{$sustentabilidade[3]->titulo}}">
							<h2>{{$sustentabilidade[3]->titulo}}</h2>
						</a>
					@endif
				</div>
				<div class="pure-u-1-2">
					@if(isset($sustentabilidade[4]))
						<a href="sustentabilidade/{{$sustentabilidade[4]->slug}}" title="{{$sustentabilidade[4]->titulo}}">
							<img src="assets/images/sustentabilidade/thumbs/{{$sustentabilidade[4]->imagem}}" alt="{{$sustentabilidade[4]->titulo}}">
							<h2>{{$sustentabilidade[4]->titulo}}</h2>
						</a>
					@endif
				</div>
			</div>
		</div>
	</div>

@stop
