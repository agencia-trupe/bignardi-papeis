@section('conteudo')

	<h1 class="page-title">Certifications</h1>

	<div class="pure-g-r listaCertificacoes outras-linguas">

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-fsc.jpg" alt="Chain of Custody FSC">
					</div>
					<div class="cke">
						<strong>Chain of Custody FSC</strong> – FSC is an independent, non-governmental and profitless organization, established to promote the responsible handling of forests worldwide. FSC Brasil has the main purpose of disseminating and facilitating the good handling of the Brazilian forests based on principles and criteria established. Bignardi uses forest inputs certified by the body, which assures products with the FSC seal within the chain of custody. The company is certified with this important seal since 2009.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-pefc.png" alt="Chain of Custody Cerflor/PEFC">
					</div>
					<div class="cke">
						<strong>Chain of Custody Cerflor/PEFC</strong> – Brazilian certification program that encompasses a set of standards for forest handling and chain of custody. It is managed by INMETRO (National Institute of Metrology, Normalization and Industry Quality) and acknowledged by the program for the forest certification granted by PEFC – a non-governmental organization dedicated to promoting forest handling practices that meet ecological, social and ethical principles.  As it uses forest-based raw materials, Bignardi is certified for the chain of custody since 2011, thus assuring the purchase of materials originated from certified forests and applying the required standards in its production chain.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-iso.png" alt="ISO 9001 – Quality Management System">
					</div>
					<div class="cke">
						<strong>ISO 9001 – Quality Management System</strong> – This is a set of standards acknowledged worldwide, which establishes the requirements for the Quality Management System at Bignardi, adding credibility to its internal processes and manufactured products.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-abnt.png" alt="ABNT NBR 15755">
					</div>
					<div class="cke">
						<strong>ABNT NBR 15755</strong> – Recycled Fiber Content – This standard sets forth the minimum requirements in order that the recycled paper aimed to printing and writing purposes uses fibers recovered from pre- and post-consumption paper waste.   Bignardi is certified since 2011, and is the first company in Brazil to apply for this certification.
					</div>
				</div>
			</div>

	</div>

@stop
