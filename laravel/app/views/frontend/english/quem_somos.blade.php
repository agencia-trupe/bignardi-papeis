@section('conteudo')

	<div class="pure-g root-grid cke texto-outras-linguas">
		<div class="pure-u-1">
			<h1>History</h1>
		</div>
		<div class="pure-u-1-2">
			<p>
				Bignardi Papéis is located n Jundiai, a city situated 60 kilometers from the capital of São Paulo.
			</p>
			<p>
				With the name of Gordinho Braune & Cia., its foundation took place in 1915, by Dr, Antonio Gordinho and engineer Herman Braune, transforming it into one of the main players of the region’s industrial progress. It started its activities as a paper dealer and in 1925, through the acquisition of its first paper machine (2nd equipment manufactured by Voith in Brazil), started up the manufacture of its product portfolio.
			</p>
			<img src="assets/images/outras-linguas/imagem-historia.png" alt="Bignardi Papéis" class="img-left">
		</div>
		<div class="pure-u-1-2">
			<p>
				In 1927, it began to use eucalyptus cellulose for the manufacture of its papers, a huge technological innovation for that time, and which currently, dominates the Brazilian paper production process.
			</p>
			<p>
				Trying to diversify its business, in 1972, Indústria GRÁFICA JANDAIA purchased Gordinho Braune, and from then on, it has been diversifying its products in the production of printing and writing papers.
			</p>
			<p>
				In 2003, it begins to be called Bignardi Papéis, which along with the brands Cadernos Jandaia and Atacado Jandaia, constitute the Bignardi Group.
			</p>
		</div>
		<div class="pure-u-1">
			<img src="assets/images/outras-linguas/banner-historia.jpg" alt="Bignardi Papéis">
		</div>
	</div>

@stop