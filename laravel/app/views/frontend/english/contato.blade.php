@section('conteudo')

    <h1 class="page-title">Contact</h1>

    <div class="pure-g-r">

        <div class="pure-u-11-24">

            <h2>55 11 4442-8352</h2>
            <span class="email-link">Rosana Albanez de Barros</span>

            <form action="{{URL::route('contato.enviarAlternativo')}}" method="post">
                @if(Session::has('enviado'))
                    <div class="success">Your message was sent successfully!</div>
                @endif
                <label>
                    <span>name</span>
                    <input type="text" name="nome" id="input-nome" required>
                </label>
                <label>
                    <span>e-mail</span>
                    <input type="email" name="email" id="input-email" required>
                </label>
                <label>
                    <span>phone</span>
                    <input type="text" name="telefone" id="input-telefone">
                </label>
                <label>
                    <span>message</span>
                    <textarea name="mensagem" id="input-mensagem" required></textarea>
                </label>

                <input type="submit" value="SEND &raquo;">
            </form>

        </div>

        <div class="pure-u-13-24">

            <div class="pad">
                <div class="endereco">
                    {{nl2br($contato->endereco)}}
                </div>

                {{Tools::viewGMaps($contato->google_maps, '100%', '450px')}}
            </div>

        </div>

    </div>

@stop
