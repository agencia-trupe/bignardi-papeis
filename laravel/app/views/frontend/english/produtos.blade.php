@section('conteudo')

	<h1 class="page-title">Products</h1>

	<p class="intro">Select by category:</p>

	@if(sizeof($categorias))
		<div class="index-categorias pure-g-r">
			@foreach($categorias as $k => $cat)
				<div class="pure-u-1-3">
					<div class="categoria">
						<div class="pre"></div>
						<div class="titulo">{{$cat->titulo_en}}</div>
						<div class="imagem">
							<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo_en}}">
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="english/products/{{$prod->slug}}" title="{{$prod->titulo_en}}">{{$prod->titulo_en}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@stop