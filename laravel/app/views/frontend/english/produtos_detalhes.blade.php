@section('conteudo')

	<div class="page-title">Products</div>


	<h2 class="titulo-categoria">{{$produto->categoria->titulo_en}}</h2>

	<h2 class="titulo-produto">{{$produto->titulo_en}}</h2>

	<div class="pure-g-r detalhes">

		<div class="pure-u-2-5">
			<div class="imagem">
				<a href="assets/images/produtos/{{$produto->imagem}}" rel="galeria" class="shadowProduto" title="Galeria de Imagens - {{$produto->titulo_en}}">
					<img class="thumb" src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo_en}}">
					<div class="overlay">
						@if(sizeof($produto->imagens))
							<img src="assets/images/layout/shadow/icone-mais.png">
						@endif
						<img src="assets/images/layout/shadow/icone-lupa.png">
					</div>
				</a>
				@if(sizeof($produto->imagens))
					@foreach($produto->imagens as $img)
						<a href="assets/images/produtosimagens/{{$img->imagem}}" class="shadowProduto" rel="galeria" style="display:none;" title="Galeria de Imagens - {{$produto->titulo_en}}"></a>
					@endforeach
				@endif
			</div>
		</div>

		<div class="pure-u-2-5">
			<div class="central texto-meio">
				@if($produto->tipo_en)
					<h3>type</h3>
					<p>{{$produto->tipo_en}}</p>
				@endif

				@if($produto->descricao_en)
					<h3>description</h3>
					<p>{{nl2br($produto->descricao_en)}}</p>
				@endif

				@if($produto->tipo_impressao_en)
					<h3>printing type</h3>
					<p>{{$produto->tipo_impressao_en}}</p>
				@endif

				@if($produto->aplicacoes_en)
					<h3>applications</h3>
					<p>{{nl2br($produto->aplicacoes_en)}}</p>
				@endif

				@if(sizeof($produto->certificacoes))
					<h3>certification</h3>
					<div class="pad-cert">
						@foreach($produto->certificacoes as $i => $cert)
							<img src="assets/images/produtoscertificacoes/{{$cert->imagem}}" alt="{{$cert->titulo_en}}" title="{{$cert->imagem}}">
						@endforeach
					</div>
				@endif
			</div>
		</div>
		<div class="pure-u-1-5">
			<div class="lateral">
				@if($produto->ph_en)
					<h3>ph</h3>
					<p>{{$produto->ph_en}}</p>
				@endif

				@if(sizeof($produto->cores))
					<h3>color</h3>
					<div class="contemCores">
						@foreach($produto->cores as $i => $cor)
							<div class="cor"><div class="bullet" style="background-color:{{$cor->hex}}"></div>	{{$cor->titulo_en}}</div>
						@endforeach
					</div>
				@endif

				@if($produto->gramatura_en)
					<h3>weight</h3>
					<p>{{$produto->gramatura_en}}</p>
				@endif

				@if($produto->folhas_en)
					<h3>sheets</h3>
					<p>{{$produto->folhas_en}}</p>
				@endif

				@if($produto->bobinas_en)
					<h3>bobbins</h3>
					<p>{{nl2br($produto->bobinas_en)}}</p>
				@endif
			</div>
		</div>
	</div>

	@if(sizeof($outros))
		<div class="outros">
			<p class="intro">Other products:</p>

			<ul>
				@foreach($outros as $outroprod)
					<li><a href="english/products/{{$outroprod->slug}}" title="{{$outroprod->titulo_en}}">{{$outroprod->titulo_en}}</a></li>
				@endforeach
			</ul>
		</div>
	@endif

	@if(sizeof($categorias))
		<div class="hide-mobile">
			<p class="intro">Select by category:</p>
			<div class="interna-lista-categorias pure-g">
				@foreach($categorias as $k => $cat)
					<div class="pure-u-1-6">
						<div class="categoria">
							<div class="pre"></div>
							<div class="titulo">{{$cat->titulo_en}}</div>
							<div class="imagem">
								<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo_en}}">
							</div>
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="english/products/{{$prod->slug}}" @if($prod->id == $produto->id) class="ativo" @endif title="{{$prod->titulo_en}}">{{$prod->titulo_en}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="hide-desktop hide-tablet">
			<a href="english/products" title="Selecionar Outras Categorias" class="link-voltar-categorias">&laquo; SELECIONAR OUTRAS CATEGORIAS</a>
		</div>
	@endif

@stop