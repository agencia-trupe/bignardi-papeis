@section('conteudo')

	<h1 class="page-title">Produtos</h1>

	<p class="intro">Selecione por categoria:</p>

	@if(sizeof($categorias))
		<div class="index-categorias pure-g-r">
			@foreach($categorias as $k => $cat)
				<div class="pure-u-1-3">
					<div class="categoria">
						<div class="pre">PAPÉIS</div>
						<div class="titulo">{{$cat->titulo}}</div>
						<div class="imagem">
							<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo}}">
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="produtos/{{$prod->slug}}" title="{{$prod->titulo}}">{{$prod->titulo}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@stop
