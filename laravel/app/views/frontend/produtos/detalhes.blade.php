@section('conteudo')

	<div class="page-title">Produtos</div>


	<h2 class="titulo-categoria">{{$produto->categoria->titulo}}</h2>

	<h2 class="titulo-produto">{{$produto->titulo}}</h2>

	<div class="pure-g-r detalhes">

		<div class="pure-u-2-5">
			<div class="imagem">
				<a href="assets/images/produtos/{{$produto->imagem}}" rel="galeria" class="shadowProduto" title="Galeria de Imagens - {{$produto->titulo}}">
					<img class="thumb" src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo}}">
					<div class="overlay">
						@if(sizeof($produto->imagens))
							<img src="assets/images/layout/shadow/icone-mais.png">
						@endif
						<img src="assets/images/layout/shadow/icone-lupa.png">
					</div>
				</a>
				@if(sizeof($produto->imagens))
					@foreach($produto->imagens as $img)
						<a href="assets/images/produtosimagens/{{$img->imagem}}" class="shadowProduto" rel="galeria" style="display:none;" title="Galeria de Imagens - {{$produto->titulo}}"></a>
					@endforeach
				@endif
			</div>
		</div>

		<div class="pure-u-2-5">
			<div class="central texto-meio">
				@if($produto->tipo)
					<h3>tipo</h3>
					<p>{{$produto->tipo}}</p>
				@endif

				@if($produto->descricao)
					<h3>descrição</h3>
					<p>{{nl2br($produto->descricao)}}</p>
				@endif

				@if($produto->tipo_impressao)
					<h3>tipo de impressão</h3>
					<p>{{$produto->tipo_impressao}}</p>
				@endif

				@if($produto->aplicacoes)
					<h3>aplicações</h3>
					<p>{{nl2br($produto->aplicacoes)}}</p>
				@endif

				@if(sizeof($produto->certificacoes))
					<h3>certificação</h3>
					<div class="pad-cert">
						@foreach($produto->certificacoes as $i => $cert)
							<img src="assets/images/produtoscertificacoes/{{$cert->imagem}}" alt="{{$cert->titulo}}" title="{{$cert->imagem}}">
						@endforeach
					</div>
				@endif
			</div>
		</div>
		<div class="pure-u-1-5">
			<div class="lateral">
				@if($produto->ph)
					<h3>ph</h3>
					<p>{{$produto->ph}}</p>
				@endif

				@if(sizeof($produto->cores))
					<h3>cor</h3>
					<div class="contemCores">
						@foreach($produto->cores as $i => $cor)
							<div class="cor"><div class="bullet" style="background-color:{{$cor->hex}}"></div>	{{$cor->titulo}}</div>
						@endforeach
					</div>
				@endif

				@if($produto->gramatura)
					<h3>gramatura</h3>
					<p>{{$produto->gramatura}}</p>
				@endif

				@if($produto->folhas)
					<h3>folhas</h3>
					<p>{{$produto->folhas}}</p>
				@endif

				@if($produto->bobinas)
					<h3>bobinas</h3>
					<p>{{nl2br($produto->bobinas)}}</p>
				@endif				
			</div>
		</div>
	</div>

	@if(sizeof($outros))
		<div class="outros">
			<p class="intro">Mais produtos desta categoria:</p>

			<ul>
				@foreach($outros as $outroprod)
					<li><a href="produtos/{{$outroprod->slug}}" title="{{$outroprod->titulo}}">{{$outroprod->titulo}}</a></li>
				@endforeach
			</ul>
		</div>
	@endif

	@if(sizeof($categorias))
		<div class="hide-mobile">
			<p class="intro">Selecione por categoria:</p>
			<div class="interna-lista-categorias pure-g">
				@foreach($categorias as $k => $cat)
					<div class="pure-u-1-6">
						<div class="categoria">
							<div class="pre">PAPÉIS</div>
							<div class="titulo">{{$cat->titulo}}</div>
							<div class="imagem">
								<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo}}">
							</div>
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="produtos/{{$prod->slug}}" @if($prod->id == $produto->id) class="ativo" @endif title="{{$prod->titulo}}">{{$prod->titulo}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="hide-desktop hide-tablet">
			<a href="produtos" title="Selecionar Outras Categorias" class="link-voltar-categorias">&laquo; SELECIONAR OUTRAS CATEGORIAS</a>
		</div>
	@endif

@stop