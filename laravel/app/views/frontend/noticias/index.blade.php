@section('conteudo')

	<h1 class="page-title">Notícias</h1>

	<div class="pure-g-r">
		
		<aside class="pure-u-1-3">
			<div id="lateral">
				@if(sizeof($listaNoticias))
					<ul>
						<?php $c=0;?>
						@foreach($listaNoticias as $i => $not)
							
							@if($c > 0 && $c%3 == 0)
								</ul><ul>
								<?php $c=0;?>
							@endif
							
							<li>
								<a href="noticias/{{$not->slug}}" title="{{$not->titulo}}" @if($not->id == $detalhe->id) class="ativo" @endif>
									<div class="data">{{Tools::formataDataNoticias($not->data)}}</div>
									<h2>{{$not->titulo}}</h2>
									<p class="olho">{{$not->olho}}</p>
								</a>
							</li>

							<?php $c++ ?>


						@endforeach
					</ul>
				@endif
			</div>

			<a href="#" id="btn-ver-mais" title="Ver Mais">VER MAIS &raquo;</a>

		</aside>

		<section class="pure-u-2-3">

			<div class="detalhe">

				@if($detalhe)

					<h1>{{$detalhe->titulo}}</h1>

					@if($detalhe->imagem)
						<img src="assets/images/noticias/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}">
					@endif
					<div class="data">{{Tools::formataDataNoticias($detalhe->data)}}</div>
					<div class="cke">
						{{$detalhe->texto}}
					</div>

				@else
					<h2>Notícia não encontrada</h2>
				@endif

			</div>

		</section>

	</div>

@stop
