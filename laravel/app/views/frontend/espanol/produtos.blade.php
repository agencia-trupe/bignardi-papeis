@section('conteudo')

	<h1 class="page-title">Productos</h1>

	<p class="intro">Seleccionar por categoría:</p>

	@if(sizeof($categorias))
		<div class="index-categorias pure-g-r">
			@foreach($categorias as $k => $cat)
				<div class="pure-u-1-3">
					<div class="categoria">
						<div class="pre"></div>
						<div class="titulo">{{$cat->titulo_es}}</div>
						<div class="imagem">
							<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo_es}}">
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="espanol/productos/{{$prod->slug}}" title="{{$prod->titulo_es}}">{{$prod->titulo_es}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@stop
