@section('conteudo')

    <h1 class="page-title">Contacto</h1>

    <div class="pure-g-r">

        <div class="pure-u-11-24">

            <h2>55 11 4442-8352</h2>
            <span class="email-link">Rosana Albanez de Barros</span>

            <form action="{{URL::route('contato.enviarAlternativo')}}" method="post">
                @if(Session::has('enviado'))
                    <div class="success">Mensaje enviado con éxito!</div>
                @endif
                <label>
                    <span>nombre</span>
                    <input type="text" name="nome" id="input-nome" required>
                </label>
                <label>
                    <span>correo electrónico</span>
                    <input type="email" name="email" id="input-email" required>
                </label>
                <label>
                    <span>teléfono</span>
                    <input type="text" name="telefone" id="input-telefone">
                </label>
                <label>
                    <span>mensaje</span>
                    <textarea name="mensagem" id="input-mensagem" required></textarea>
                </label>

                <input type="submit" value="ENVIAR &raquo;">
            </form>

        </div>

        <div class="pure-u-13-24">

            <div class="pad">
                <div class="endereco">
                    {{nl2br($contato->endereco)}}
                </div>

                {{Tools::viewGMaps($contato->google_maps, '100%', '450px')}}
            </div>

        </div>

    </div>

@stop
