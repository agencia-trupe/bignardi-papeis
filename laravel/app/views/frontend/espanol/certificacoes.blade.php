@section('conteudo')

	<h1 class="page-title">Certificaciones</h1>

	<div class="pure-g-r listaCertificacoes outras-linguas">

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-fsc.jpg" alt="Cadena de custodia FSC">
					</div>
					<div class="cke">
						<strong>Cadena de custodia FSC</strong> – FSC es una organización independiente, no gubernamental y sin fines de lucro, establecida para promover el manejo responsable de los bosques en el mundo. FSC Brasil tiene como objetivo principal difundir y facilitar el buen manejo de los bosques brasileños a través de principios y criterios establecidos. Bignardi utiliza insumos forestales certificados por el organismo, que garantiza dentro de la cadena de custodia, productos con el sello FSC. La empresa está certificada con este importante sello desde el año 2009.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-pefc.png" alt="Cadena de custodia Cerflor/PEFC">
					</div>
					<div class="cke">
						<strong>Cadena de custodia Cerflor/PEFC</strong> – Programa brasileño de certificación que consiste en un conjunto de normas para el manejo de los bosques y para la cadena de custodia. Está administrado por el Instituto Nacional de Metrología, Normalización y Calidad Industrial – INMETRO y está reconocida por el Programa para el Reconocimiento de la Certificación Forestal PEFC – organización no gubernamental dedicada a promover prácticas forestales que respeten los preceptos ecológicos, sociales y éticos.  Bignardi, por el hecho de utilizar materias primas de base forestal, está certificada para la cadena de custodia desde el año 2011, garantizando la adquisición de materiales certificados de origen forestal y aplicando en su cadena productiva los estándares exigidos.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-iso.png" alt="ISO 9001 – Quality Management System">
					</div>
					<div class="cke">
						<strong>ISO 9001 – Sistema de Gestión de Calidad</strong> – Conjunto de normas reconocidas internacionalmente que establece los requisitos para el Sistema de Gestión de Calidad de Bignardi, agregando credibilidad a los procesos internos y a los productos fabricados.
					</div>
				</div>
			</div>

			<div class="pure-u-1-4 certificacao">
				<div class="pad">
					<div class="imagem">
						<img src="assets/images/outras-linguas/certificacao-abnt.png" alt="ABNT NBR 15755">
					</div>
					<div class="cke">
						<strong>ABNT NBR 15755</strong>Contenido de Fibras Recicladas – Esta norma establece los requisitos mínimos para que el papel reciclado, destinado a la impresión y escritura utilice fibras recuperadas de desechos de papel pre y post-consumo.  Bignardi está certificada desde 2011, siendo la primera empresa en el Brasil en obtener esta certificación.
					</div>
				</div>
			</div>

	</div>

@stop
