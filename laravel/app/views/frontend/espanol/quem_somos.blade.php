@section('conteudo')

	<div class="pure-g root-grid cke texto-outras-linguas">
		<div class="pure-u-1">
			<h1>Historia</h1>
		</div>
		<div class="pure-u-1-2">
			<p>
				La Bignardi Papéis está localizada en Jundiai, ciudad situada a 60 kilómetros de la capital paulista.
			</p>
			<p>
				Con el nombre de Gordinho Braune & Cia., su fundación sucedió en 1915, por el Dr. Antonio Cintura Gordinho y el ingeniero Herman Braune, transformándose en una de las protagonistas de la evolución industrial de la región. Comenzó sus actividades como una distribuidora de papel y en 1925, a través de la adquisición de su primera máquina de papel (2º equipo fabricado por Voith en Brasil), dio comienzo a la fabricación de su cartera de productos.
			</p>
			<img src="assets/images/outras-linguas/imagem-historia.png" alt="Bignardi Papéis" class="img-left">
		</div>
		<div class="pure-u-1-2">
			<p>
				En 1927, pasó a utilizar la celulosa de eucalipto para la fabricación de sus papeles, una innovación tecnológica muy grande para la época, y que en la actualidad, domina el proceso de producción de papel brasileño.
			</p>
			<p>
				Intentando diversificar sus negocios en 1972, la Indústria Gráfica JANDAIA adquirió la Gordinho Braune, y a partir de entonces, ha estado diversificando sus productos en la producción de papeles para imprimir y escribir.
			</p>
			<p>
				En 2003, comienza a llamarse Bignardi Papéis, que juntamente con las marcas Cadernos Jandaia y Atacado Jandaia constituyen el Grupo Bignardi.
			</p>
		</div>
		<div class="pure-u-1">
			<img src="assets/images/outras-linguas/banner-historia.jpg" alt="Bignardi Papéis">
		</div>
	</div>

@stop