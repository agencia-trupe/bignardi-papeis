@section('conteudo')

	<div class="page-title">Productos</div>

	<h2 class="titulo-categoria">{{$produto->categoria->titulo_es}}</h2>

	<h2 class="titulo-produto">{{$produto->titulo_es}}</h2>

	<div class="pure-g-r detalhes">

		<div class="pure-u-2-5">
			<div class="imagem">
				<a href="assets/images/produtos/{{$produto->imagem}}" rel="galeria" class="shadowProduto" title="Galeria de Imagens - {{$produto->titulo_es}}">
					<img class="thumb" src="assets/images/produtos/thumbs/{{$produto->imagem}}" alt="{{$produto->titulo_es}}">
					<div class="overlay">
						@if(sizeof($produto->imagens))
							<img src="assets/images/layout/shadow/icone-mais.png">
						@endif
						<img src="assets/images/layout/shadow/icone-lupa.png">
					</div>
				</a>
				@if(sizeof($produto->imagens))
					@foreach($produto->imagens as $img)
						<a href="assets/images/produtosimagens/{{$img->imagem}}" class="shadowProduto" rel="galeria" style="display:none;" title="Galeria de Imagens - {{$produto->titulo_es}}"></a>
					@endforeach
				@endif
			</div>
		</div>

		<div class="pure-u-2-5">
			<div class="central texto-meio">
				@if($produto->tipo_es)
					<h3>tipo</h3>
					<p>{{$produto->tipo_es}}</p>
				@endif

				@if($produto->descricao_es)
					<h3>descripción</h3>
					<p>{{nl2br($produto->descricao_es)}}</p>
				@endif

				@if($produto->tipo_impressao_es)
					<h3>tipo de impresión</h3>
					<p>{{$produto->tipo_impressao_es}}</p>
				@endif

				@if($produto->aplicacoes_es)
					<h3>aplicaciones</h3>
					<p>{{nl2br($produto->aplicacoes_es)}}</p>
				@endif

				@if(sizeof($produto->certificacoes))
					<h3>certificación</h3>
					<div class="pad-cert">
						@foreach($produto->certificacoes as $i => $cert)
							<img src="assets/images/produtoscertificacoes/{{$cert->imagem}}" alt="{{$cert->titulo_es}}" title="{{$cert->imagem}}">
						@endforeach
					</div>
				@endif
			</div>
		</div>
		<div class="pure-u-1-5">
			<div class="lateral">
				@if($produto->ph_es)
					<h3>ph</h3>
					<p>{{$produto->ph_es}}</p>
				@endif

				@if(sizeof($produto->cores))
					<h3>color</h3>
					<div class="contemCores">
						@foreach($produto->cores as $i => $cor)
							<div class="cor"><div class="bullet" style="background-color:{{$cor->hex}}"></div>	{{$cor->titulo_es}}</div>
						@endforeach
					</div>
				@endif

				@if($produto->gramatura_es)
					<h3>peso</h3>
					<p>{{$produto->gramatura_es}}</p>
				@endif

				@if($produto->folhas_es)
					<h3>hojas</h3>
					<p>{{$produto->folhas_es}}</p>
				@endif

				@if($produto->bobinas_es)
					<h3>bobinas</h3>
					<p>{{nl2br($produto->bobinas_es)}}</p>
				@endif
			</div>
		</div>
	</div>

	@if(sizeof($outros))
		<div class="outros">
			<p class="intro">Otros productos:</p>

			<ul>
				@foreach($outros as $outroprod)
					<li><a href="espanol/productos/{{$outroprod->slug}}" title="{{$outroprod->titulo_es}}">{{$outroprod->titulo_es}}</a></li>
				@endforeach
			</ul>
		</div>
	@endif

	@if(sizeof($categorias))
		<div class="hide-mobile">
			<p class="intro">Seleccionar por categoría:</p>
			<div class="interna-lista-categorias pure-g">
				@foreach($categorias as $k => $cat)
					<div class="pure-u-1-6">
						<div class="categoria">
							<div class="pre"></div>
							<div class="titulo">{{$cat->titulo_es}}</div>
							<div class="imagem">
								<img src="assets/images/categorias/{{$cat->imagem}}" alt="{{$cat->titulo_es}}">
							</div>
						</div>
						<div class="produtos">
							@if(sizeof($cat->produtos))
								<ul>
								@foreach($cat->produtos as $i => $prod)
									<li><a href="espanol/productos/{{$prod->slug}}" @if($prod->id == $produto->id) class="ativo" @endif title="{{$prod->titulo_es}}">{{$prod->titulo_es}}</a></li>
								@endforeach
								</ul>
							@endif
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="hide-desktop hide-tablet">
			<a href="espanol/productos" title="Selecionar Outras Categorias" class="link-voltar-categorias">&laquo; SELECT OTRAS CATEGORÍAS</a>
		</div>
	@endif

@stop