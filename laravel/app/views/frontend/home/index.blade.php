@section('conteudo')

	<div id="banners" class="hide-mobile hide-tablet">
		@if(sizeof($banners))
			<div id="ampliadas">
				@foreach($banners as $k => $banner)
					<a href="{{$banner->link}}" title="{{$banner->titulo}}" class="banner @if($k==0) aberto @endif" data-id="{{$banner->id}}">
						<img src="assets/images/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}">
						<div class="texto">
							<h1>{{$banner->titulo}}</h1>
							<p>{{$banner->texto}}</p>
						</div>
					</a>
				@endforeach
			</div>
			<div id="thumbs">
				@foreach($banners_reverso as $k => $banner)
					<a href="#" data-target="{{$banner->id}}" title="{{$banner->titulo}}" class="thumb @if($k==3) aberto suma @endif">
						<img src="assets/images/banners/thumbs/{{$banner->imagem}}" alt="{{$banner->titulo}}">
						<div class="texto">
							<h1>{{$banner->titulo}}</h1>							
						</div>
					</a>
				@endforeach
			</div>
		@endif
	</div>

	<div id="banners-tablet" class="hide-desktop hide-mobile">
		@if(sizeof($banners))
			<div id="ampliadas">
				@foreach($banners as $k => $banner)
					<a href="{{$banner->link}}" title="{{$banner->titulo}}" class="banner @if($k==0) aberto @endif" data-id="{{$banner->id}}">
						<img src="assets/images/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}">
						<div class="texto">
							<h1>{{$banner->titulo}}</h1>
							<p>{{$banner->texto}}</p>
						</div>
					</a>
				@endforeach
			</div>
			<div id="thumbs">
				@foreach($banners_reverso as $k => $banner)
					<a href="#" data-target="{{$banner->id}}" title="{{$banner->titulo}}" class="thumb @if($k==3) aberto suma @endif">
						<img src="assets/images/banners/thumbs/{{$banner->imagem}}" alt="{{$banner->titulo}}">
						<div class="texto">
							<h1>{{$banner->titulo}}</h1>							
						</div>
					</a>
				@endforeach
			</div>
		@endif
	</div>

	<div id="banner-mobile" class="hide-desktop hide-tablet pure-g">
		@foreach($banners_mobile as $k => $banner)
			<div class="pure-u-1-3">
				<a href="{{$banner->link}}" title="{{$banner->titulo}}">
					<img src="assets/images/banners/thumbs/{{$banner->imagem}}" alt="{{$banner->titulo}}">
					<div class="texto">
						<h1>{{$banner->titulo}}</h1>							
					</div>
				</a>
			</div>
		@endforeach
	</div>

	<div id="chamadas" class="pure-g">
		@if($noticias)
			@foreach($noticias as $noticia)
				<div class="chamada pure-u-1-3">
					<a href="noticias/{{$noticia->slug}}" title="{{$noticia->titulo}}">
						<div class="data">{{Tools::formataDataNoticias($noticia->data)}}</div>
						<h1>{{$noticia->titulo}}</h1>
						<p>{{$noticia->olho}}</p>
					</a>
				</div>
			@endforeach
		@endif
	</div>

@stop
