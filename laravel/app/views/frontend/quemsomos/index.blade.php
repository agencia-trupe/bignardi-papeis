@section('conteudo')
	
	<div class="pure-g root-grid">
		<div class="pure-u-1-2 quebrar">
			<div class="historia cke">
				<h1>História</h1>
				{{$quemsomos->historia}}
			</div>
		</div>
		<div class="pure-u-1-2 quebrar">
			<div class="pure-g">
				<div class="pure-u-1-2 fundoAzul">
					<div class="missao cke">
						<h1>Missão</h1>
						{{$quemsomos->missao}}
					</div>
				</div>
				<div class="pure-u-1-2 fundoAzul">
					<div class="visao cke">
						<h1>Visão</h1>
						{{$quemsomos->visao}}
					</div>
				</div>
				<div class="pure-u-1 fundoAzul">
					<div class="valores cke">
						<h1>Valores</h1>
						{{$quemsomos->valores}}
					</div>
				</div>
			</div>
		</div>
		<div class="pure-u-1">
			<div class="imagem">
				<div class="texto-overlay cke">
					{{$quemsomos->texto_imagem}}
				</div>
				<img src="assets/images/quemsomos/{{$quemsomos->imagem}}" alt="Bignardi Papéis">
			</div>
		</div>
	</div>

@stop
