@section('conteudo')
	
	<h1 class="page-title">Representantes</h1>

	<div class="pure-g-r">

		<div class="pure-u-2-5">
			<div id="mapa" class="hide-mobile">
				<a href="representantes/ac" class="ac @if($marcarEstado=='ac') active @endif" title="Acre" data-uf="AC">Acre</a>
				<a href="representantes/al" class="al @if($marcarEstado=='al') active @endif" title="Alagoas" data-uf="AL">Alagoas</a>
				<a href="representantes/ap" class="ap @if($marcarEstado=='ap') active @endif" title="Amapá" data-uf="AP">Amapá</a>
				<a href="representantes/am" class="am @if($marcarEstado=='am') active @endif" title="Amazonas" data-uf="AM">Amazonas</a>
				<a href="representantes/ba" class="ba @if($marcarEstado=='ba') active @endif" title="Bahia " data-uf="BA">Bahia </a>
				<a href="representantes/ce" class="ce @if($marcarEstado=='ce') active @endif" title="Ceará" data-uf="CE">Ceará</a>
				<a href="representantes/df" class="df @if($marcarEstado=='df') active @endif" title="Distrito Federal" data-uf="DF">Distrito Federal</a>
				<a href="representantes/es" class="es @if($marcarEstado=='es') active @endif" title="Espírito Santo" data-uf="ES">Espírito Santo</a>
				<a href="representantes/go" class="go @if($marcarEstado=='go') active @endif" title="Goiás" data-uf="GO">Goiás</a>
				<a href="representantes/ma" class="ma @if($marcarEstado=='ma') active @endif" title="Maranhão" data-uf="MA">Maranhão</a>
				<a href="representantes/mt" class="mt @if($marcarEstado=='mt') active @endif" title="Mato Grosso" data-uf="MT">Mato Grosso</a>
				<a href="representantes/ms" class="ms @if($marcarEstado=='ms') active @endif" title="Mato Grosso do Sul" data-uf="MS">Mato Grosso do Sul</a>
				<a href="representantes/mg" class="mg @if($marcarEstado=='mg') active @endif" title="Minas Gerais" data-uf="MG">Minas Gerais</a>
				<a href="representantes/pa" class="pa @if($marcarEstado=='pa') active @endif" title="Pará" data-uf="PA">Pará</a>
				<a href="representantes/pb" class="pb @if($marcarEstado=='pb') active @endif" title="Paraíba" data-uf="PB">Paraíba</a>
				<a href="representantes/pr" class="pr @if($marcarEstado=='pr') active @endif" title="Paraná" data-uf="PR">Paraná</a>
				<a href="representantes/pe" class="pe @if($marcarEstado=='pe') active @endif" title="Pernambuco" data-uf="PE">Pernambuco</a>
				<a href="representantes/pi" class="pi @if($marcarEstado=='pi') active @endif" title="Piauí" data-uf="PI">Piauí</a>
				<a href="representantes/rj" class="rj @if($marcarEstado=='rj') active @endif" title="Rio de Janeiro" data-uf="RJ">Rio de Janeiro</a>
				<a href="representantes/rn" class="rn @if($marcarEstado=='rn') active @endif" title="Rio Grande do Norte" data-uf="RN">Rio Grande do Norte</a>
				<a href="representantes/rs" class="rs @if($marcarEstado=='rs') active @endif" title="Rio Grande do Sul" data-uf="RS">Rio Grande do Sul</a>
				<a href="representantes/ro" class="ro @if($marcarEstado=='ro') active @endif" title="Rondônia" data-uf="RO">Rondônia</a>
				<a href="representantes/rr" class="rr @if($marcarEstado=='rr') active @endif" title="Roraima" data-uf="RR">Roraima</a>
				<a href="representantes/sc" class="sc @if($marcarEstado=='sc') active @endif" title="Santa Catarina" data-uf="SC">Santa Catarina</a>
				<a href="representantes/sp" class="sp @if($marcarEstado=='sp') active @endif" title="São Paulo" data-uf="SP">São Paulo</a>
				<a href="representantes/se" class="se @if($marcarEstado=='se') active @endif" title="Sergipe" data-uf="SE">Sergipe</a>
				<a href="representantes/to" class="to @if($marcarEstado=='to') active @endif" title="Tocantins" data-uf="TO">Tocantins</a>
			</div>
		</div>

		<div class="pure-u-3-5">
			<h2>ENCONTRE UM REPRESENTANTE</h2>

			<p>Selecione o Estado para visualizar a lista de representantes próximos a você:</p>

			<p>Estado:</p>
			<form action="representantes" method="post" id="representante-form">
				<select name="estado_post" id="select-estado" required>
					<option value="">Selecione...</option>
					@if(sizeof($listaEstados))
						@foreach($listaEstados as $estado)
							<option value="{{$estado->uf}}" @if($marcarEstado==strtolower($estado->uf)) selected @endif>{{$estado->nome}}</option>
						@endforeach
					@endif
				</select>
				<input type="submit" value="PESQUISAR">
			</form>
		</div>


	</div>

	@if($listaRepresentantes != false && sizeof($listaRepresentantes))
		<div class="pure-g-r listaRepresentantes">
			@foreach($listaRepresentantes as $representante)
				<div class="pure-u-1-2">
					<div class="pad">
						<div class="representante">
							@if($representante->regiao)
								Região: <strong>{{ $representante->regiao }}</strong><br><br>
							@endif
							@if($representante->empresa)
								{{ $representante->empresa }}<br>
							@endif
							{{ $representante->texto }}
							@if($representante->email)
								E-mail: <a href="mailto:{{$representante->email}}" title="Entre em contato!">{{ $representante->email }}</a>
							@endif
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif

@stop
