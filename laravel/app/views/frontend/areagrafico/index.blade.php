@section('conteudo')
	<h1 class="page-title">Área do Gráfico</h1>

	<div class="pure-g-r">
		<div class="pure-u-7-24 intro">
			<p>
				Aqui o profissional da área gráfica encontra materiais de suporte para a realização de trabalhos utilizando papéis e suprimentos da Bignardi Papéis.
			</p>
			<p>
				Aproveite!
			</p>
		</div>
		<div class="pure-u-17-24">
			<div class="listaArea">
				@if(sizeof($textos))
					@foreach($textos as $texto)
						<a href="areagrafico/{{$texto->slug}}" title="{{$texto->titulo}}" class="area-index-texto">
							<h2>{{$texto->titulo}}</h2>
							<p>{{$texto->descricao}}</p>
							<div class="overlay"><div class="pad">Visualizar</div></div>
						</a>
					@endforeach
				@endif
				@if(sizeof($arquivos))
					@foreach($arquivos as $arquivo)
						<a href="download/{{$arquivo->arquivo}}" title="{{$arquivo->titulo}}" class="area-index-arquivo">
							<h2>{{$arquivo->titulo}}</h2>
							<p>{{$arquivo->descricao}}</p>
							<div class="overlay"><div class="pad">Fazer download do material</div></div>
						</a>
					@endforeach
				@endif
			</div>
		</div>
	</div>
@stop
