@section('conteudo')
	<h1 class="page-title">Área do Gráfico</h1>

	<div class="pure-g-r">
		<div class="pure-u-7-24 intro">
			<p>
				Aqui o profissional da área gráfica encontra materiais de suporte para a realização de trabalhos utilizando papéis e suprimentos da Bignardi Papéis.
			</p>
			<p>
				Aproveite!
			</p>

			<a href="{{URL::route('areagrafico')}}" title="Voltar" class="btn-voltar">&larr; voltar</a>
		</div>
		<div class="pure-u-17-24">
			<div class="texto" id="lista-area-texto">
				@if(sizeof($texto))
					<h2>{{$texto->titulo}}</h2>
					@if(sizeof($texto->itens))
						@foreach($texto->itens as $item)
							<a href="#" title="{{$item->titulo}}">
								<span>{{$item->titulo}}</span>
								<div class="descricao">
									{{$item->descricao}}
								</div>
							</a>
						@endforeach
					@endif
				@else
					<h2>Texto não encontrado</h2>
				@endif
			</div>
		</div>
	</div>
@stop
