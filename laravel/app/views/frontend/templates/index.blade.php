<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="keywords" content="" />

	<title>Bignardi Papéis</title>
	<meta name="description" content="">
	<meta property="og:title" content="Bignardi Papéis"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Bignardi Papéis"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/fancybox/source/helpers/jquery.fancybox-thumbs',
		'vendor/pure/grids-min',
		'css/simpleselect',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr.js', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr.js'))?>
	@endif

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>

	<div class="centro topo">

		@if(str_is('english*', Route::currentRouteName()) || str_is('espanol*', Route::currentRouteName()))
			<header class="outras-linguas">
		@else
			<header>
		@endif
			<a href="home" title="Página Inicial" id="link-home"><img src="assets/images/layout/marca-bignardi-papeis.png" alt="Bignardi Papéis"></a>
			<nav>
				<ul class="menu-superior">
					@if(str_is('english*', Route::currentRouteName()))
						<li><a href="{{URL::route('english.quemsomos')}}" title="Who We Are" @if(str_is('*quemsomos*', Route::currentRouteName())) class='ativo' @endif> <span>WHO WE ARE</span></a></li>
						<li><a href="{{URL::route('english.certificacoes')}}" title="Certifications" @if(str_is('*certificacoes*', Route::currentRouteName())) class='ativo' @endif> <span>CERTIFICATIONS</span></a></li>
						<li><a href="{{URL::route('english.produtos')}}" title="Products" @if(str_is('*produtos*', Route::currentRouteName())) class='ativo' @endif> <span>PRODUCTS</span></a></li>
						<li><a href="{{URL::route('english.contato')}}" title="Contact" @if(str_is('*contato*', Route::currentRouteName())) class='ativo' @endif> <span>CONTACT</span></a></li>
					@elseif(str_is('espanol*', Route::currentRouteName()))
						<li><a href="{{URL::route('espanol.quemsomos')}}" title="Quien Somos" @if(str_is('*quemsomos*', Route::currentRouteName())) class='ativo' @endif> <span>QUIEN SOMOS</span></a></li>
						<li><a href="{{URL::route('espanol.certificacoes')}}" title="Certificaciones" @if(str_is('*certificacoes*', Route::currentRouteName())) class='ativo' @endif> <span>CERTIFICACIONES</span></a></li>
						<li><a href="{{URL::route('espanol.produtos')}}" title="Productos" @if(str_is('*produtos*', Route::currentRouteName())) class='ativo' @endif> <span>PRODUCTOS</span></a></li>
						<li><a href="{{URL::route('espanol.contato')}}" title="Contacto" @if(str_is('*contato*', Route::currentRouteName())) class='ativo' @endif> <span>CONTACTO</span></a></li>
					@else
						<li><a href="{{URL::route('quemsomos')}}" title="Quem Somos" @if(str_is('*quemsomos*', Route::currentRouteName())) class='ativo' @endif> <span>QUEM SOMOS</span></a></li>
						<li><a href="{{URL::route('certificacoes')}}" title="Certificações" @if(str_is('*certificacoes*', Route::currentRouteName())) class='ativo' @endif> <span>CERTIFICAÇÕES</span></a></li>
						<li><a href="{{URL::route('representantes')}}" title="Representantes" @if(str_is('representantes', Route::currentRouteName())) class='ativo' @endif> <span>REPRESENTANTES</span></a></li>
						<li><a href="{{URL::route('noticias')}}" title="Notícias" @if(str_is('noticias', Route::currentRouteName())) class='ativo' @endif> <span>NOTÍCIAS</span></a></li>
						<li><a href="{{URL::route('contato')}}" title="Contato" @if(str_is('contato', Route::currentRouteName())) class='ativo' @endif> <span>CONTATO</span></a></li>
					@endif
				</ul>
				@if(!str_is('english*', Route::currentRouteName()) && !str_is('espanol*', Route::currentRouteName()))
					<ul class="menu-inferior">
						<li><a href="{{URL::route('produtos')}}" title="Produtos" @if(str_is('produto*', Route::currentRouteName())) class='ativo' @endif> <span>PRODUTOS</span></a></li>
						<li><a href="{{URL::route('sustentabilidade')}}" title="Sustentabilidade" @if(str_is('sustentabilidade*', Route::currentRouteName())) class='ativo' @endif> <span>SUSTENTABILIDADE</span></a></li>
						<li><a href="{{URL::route('areagrafico')}}" title="Área do Gráfico" @if(str_is('areagrafico*', Route::currentRouteName())) class='ativo' @endif> <span>ÁREA DO GRÁFICO</span></a></li>
						<li><a href="{{URL::route('trabalho')}}" title="Galeria de Trabalhos" @if(str_is('trabalho', Route::currentRouteName())) class='ativo' @endif> <span>GALERIA DE TRABALHOS</span></a></li>
					</ul>
				@endif
			</nav>
			<div id="header-contato">
				@if($contato->telefone)
					<div class="telefone">SAC {{$contato->telefone}}</div>
				@endif
				@if($contato->email)
					<a href="mailto:{{$contato->email}}" title="Envie um e-mail" class="email-link-topo">{{$contato->email}}</a>
				@endif
				@if($contato->facebook)
					<a href="{{$contato->facebook}}" target="_blank" title="Facebook" class="facebook-link">{{$contato->facebook}}</a>
				@endif
				@if($contato->twitter)
					<a href="{{$contato->twitter}}" target="_blank" title="Twitter" class="twitter-link">{{$contato->twitter}}</a>
				@endif
                <span style="display:inline-block;height:16px;line-height:16px;margin:0 3px;font-size:22px;color:#a6e5ee;">·</span>

                @if(str_is('english*', Route::currentRouteName()))
                	<a href=""><img src="assets/images/layout/lang-pt.png" style="display:inline-block;" alt="Versão em Português"></a>
                	<a href="espanol"><img src="assets/images/layout/lang-es.png" style="display:inline-block;" alt="Versión en Español"></a>
				@elseif(str_is('espanol*', Route::currentRouteName()))
					<a href="english"><img src="assets/images/layout/lang-en.png" style="display:inline-block;" alt="English Version"></a>
                	<a href=""><img src="assets/images/layout/lang-pt.png" style="display:inline-block;" alt="Versão em Português"></a>
				@else
                	<a href="english"><img src="assets/images/layout/lang-en.png" style="display:inline-block;" alt="English Version"></a>
                	<a href="espanol"><img src="assets/images/layout/lang-es.png" style="display:inline-block;" alt="Versión en Español"></a>
				@endif
			</div>
		</header>

	</div>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		<div class="centro">
			@yield('conteudo')
		</div>
	</div>

	<footer>
		<div class="centro">
			<div id="footer-contato">
				<a href="#" id="scroll-topo" title="Voltar ao topo">TOPO</a>
				<div class="telefone">
					@if(str_is('english*', Route::currentRouteName()) || str_is('espanol*', Route::currentRouteName()))
						55 11 4442-8352
					@else
						@if($contato->telefone) SAC {{$contato->telefone}} @endif
					@endif
				</div>
				<div class="endereco">
					@if($contato->endereco_rodape) {{nl2br($contato->endereco_rodape)}} @endif
				</div>
			</div>
			<div id="assinatura">
				&copy; {{Date('Y')}} Bignardi Papéis<br>
				@if(str_is('english*', Route::currentRouteName()))
					All Rights Reserved<br>
				@elseif(str_is('espanol*', Route::currentRouteName()))
					Reservados Todos los Derechos<br>
				@else
					Todos os direitos reservados<br>
				@endif
				@if(str_is('english*', Route::currentRouteName()) || str_is('espanol*', Route::currentRouteName()))
					<a href="http://www.trupe.net" title="Sites: Trupe Agência Criativa" target="_blank">Sites: Trupe Agência Criativa</a>
				@else
					<a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Criação de sites: Trupe Agência Criativa</a>
				@endif
			</div>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/fancybox/source/helpers/jquery.fancybox-thumbs',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/simpleselect',
		'js/main'
	))?>

</body>
</html>
