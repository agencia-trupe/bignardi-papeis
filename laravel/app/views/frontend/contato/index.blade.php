@section('conteudo')
	
	<h1 class="page-title">Contato</h1>

	<div class="pure-g-r">

		<div class="pure-u-11-24">
			
			<h2>SAC {{$contato->telefone}}</h2>
			
			@if($contato->email)
				<a href="mailto:{{$contato->email}}" class="email-link" title="Email">{{$contato->email}}</a>
			@endif

			<form action="{{URL::route('contato.enviar')}}" method="post">
				@if(Session::has('enviado'))
					<div class="success">Sua mensagem foi enviada com sucesso!</div>
				@endif
				<label>
					<span>nome</span>
					<input type="text" name="nome" id="input-nome" required>
				</label>
				<label>
					<span>e-mail</span>
					<input type="email" name="email" id="input-email" required>
				</label>
				<label>
					<span>telefone</span>
					<input type="text" name="telefone" id="input-telefone">
				</label>
				<label>
					<span>mensagem</span>
					<textarea name="mensagem" id="input-mensagem" required></textarea>
				</label>

				<input type="submit" value="ENVIAR &raquo;">
			</form>

		</div>

		<div class="pure-u-13-24">

			<div class="pad">				
				<div class="endereco">
					{{nl2br($contato->endereco)}}
				</div>

				{{Tools::viewGMaps($contato->google_maps, '100%', '450px')}}
			</div>

		</div>

	</div>
	
@stop
