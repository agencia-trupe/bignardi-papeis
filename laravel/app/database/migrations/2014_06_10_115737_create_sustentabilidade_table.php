<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSustentabilidadeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sustentabilidade', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('slug');
			$table->text('texto');
			$table->string('imagem');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sustentabilidade');
	}

}
