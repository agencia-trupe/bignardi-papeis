<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('representantes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('regiao');
			$table->text('texto');
			$table->string('email');
			$table->string('cep');
			$table->text('empresa');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('representantes');
	}

}
