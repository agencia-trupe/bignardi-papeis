<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTabelasIngles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categorias', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo');
			$table->string('titulo_es')->after('titulo_en');
		});

		Schema::table('produtos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo');
			$table->string('titulo_es')->after('titulo_en');

			$table->text('tipo_en')->after('tipo');
			$table->text('tipo_es')->after('tipo_en');

			$table->text('descricao_en')->after('descricao');
			$table->text('descricao_es')->after('descricao_en');

			$table->text('tipo_impressao_en')->after('tipo_impressao');
			$table->text('tipo_impressao_es')->after('tipo_impressao_en');

			$table->text('aplicacoes_en')->after('aplicacoes');
			$table->text('aplicacoes_es')->after('aplicacoes_en');

			$table->text('ph_en')->after('ph');
			$table->text('ph_es')->after('ph_en');

			$table->text('gramatura_en')->after('gramatura');
			$table->text('gramatura_es')->after('gramatura_en');

			$table->text('folhas_en')->after('folhas');
			$table->text('folhas_es')->after('folhas_en');

			$table->text('bobinas_en')->after('bobinas');
			$table->text('bobinas_es')->after('bobinas_en');
		});

		Schema::table('certificacoes_produtos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo');
			$table->string('titulo_es')->after('titulo_en');
		});

		Schema::table('cores', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo');
			$table->string('titulo_es')->after('titulo_en');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categorias', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('titulo_es');
		});

		Schema::table('produtos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('titulo_es');

			$table->dropColumn('tipo_en');
			$table->dropColumn('tipo_es');

			$table->dropColumn('descricao_en');
			$table->dropColumn('descricao_es');

			$table->dropColumn('tipo_impressao_en');
			$table->dropColumn('tipo_impressao_es');

			$table->dropColumn('aplicacoes_en');
			$table->dropColumn('aplicacoes_es');

			$table->dropColumn('ph_en');
			$table->dropColumn('ph_es');

			$table->dropColumn('gramatura_en');
			$table->dropColumn('gramatura_es');

			$table->dropColumn('folhas_en');
			$table->dropColumn('folhas_es');

			$table->dropColumn('bobinas_en');
			$table->dropColumn('bobinas_es');
		});

		Schema::table('certificacoes_produtos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('titulo_es');
		});

		Schema::table('cores', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('titulo_es');
		});
	}

}
