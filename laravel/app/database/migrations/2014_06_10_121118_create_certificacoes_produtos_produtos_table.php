<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificacoesProdutosProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('certificacoes_produtos_produtos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('certificacoes_produtos_id');
			$table->string('produtos_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('certificacoes_produtos_produtos');
	}

}
