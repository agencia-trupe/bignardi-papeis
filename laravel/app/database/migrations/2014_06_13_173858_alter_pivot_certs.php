<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPivotCerts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('certificacoes_produtos_produtos', function(Blueprint $table)
		{
			$table->renameColumn('certificacoes_produtos_id', 'produtos_certificacoes_id');
			$table->renameColumn('produtos_id', 'produto_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('certificacoes_produtos_produtos', function(Blueprint $table)
		{
			$table->renameColumn('produtos_certificacao_id', 'certificacoes_produtos_id');
			$table->renameColumn('produto_id', 'produtos_id');
		});
	}

}
