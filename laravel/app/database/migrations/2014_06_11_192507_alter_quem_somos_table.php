<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuemSomosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->text('visao')->after('missao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->dropColumn('visao');
		});
	}

}
