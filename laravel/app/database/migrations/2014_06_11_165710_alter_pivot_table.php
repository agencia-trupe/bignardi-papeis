<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rep_cidades_representantes', function(Blueprint $table)
		{
			$table->dropColumn('rep_estados_id');
			$table->dropColumn('rep_cidades_id');
			$table->dropColumn('representantes_id');
			$table->integer('estado_id');
			$table->integer('cidade_id');
			$table->integer('representante_id');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table->integer('rep_estados_id');
		$table->integer('rep_cidades_id');
		$table->integer('representantes_id');
		$table->dropColumn('estado_id');
		$table->dropColumn('cidade_id');
		$table->dropColumn('representante_id');
	}

}
