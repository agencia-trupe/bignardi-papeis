<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraficoTextosItensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grafico_textos_itens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('grafico_textos_id');
			$table->string('titulo');
			$table->string('slug');
			$table->text('descricao');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grafico_textos_itens');
	}

}
