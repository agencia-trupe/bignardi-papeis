<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepCidadesRepresentantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rep_cidades_representantes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rep_estados_id');
			$table->integer('rep_cidades_id');
			$table->integer('representantes_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rep_cidades_representantes');
	}

}
