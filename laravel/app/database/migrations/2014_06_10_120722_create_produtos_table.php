<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('categorias_id');
			$table->string('titulo');
			$table->string('slug');
			$table->string('imagem');
			$table->text('tipo');
			$table->text('descricao');
			$table->text('tipo_impressao');
			$table->text('aplicacoes');
			$table->text('ph');
			$table->text('gramatura');
			$table->text('folhas');
			$table->text('bobinas');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos');
	}

}
