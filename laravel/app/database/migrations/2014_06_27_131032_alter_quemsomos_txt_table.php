<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuemsomosTxtTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->text('texto_imagem')->after('valores');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->dropColumn('texto_imagem');
		});
	}

}
