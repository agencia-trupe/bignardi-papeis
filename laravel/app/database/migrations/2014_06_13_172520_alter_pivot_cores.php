<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPivotCores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cor_produto', function(Blueprint $table)
		{
			$table->renameColumn('cores_id', 'cor_id');
			$table->renameColumn('produtos_id', 'produto_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cor_produto', function(Blueprint $table)
		{
			$table->renameColumn('cor_id', 'cores_id');
			$table->renameColumn('produto_id', 'produtos_id');
		});
	}

}
