<?php

use GraficoArquivo as GraficoArquivo;
use GraficoTexto as GraficoTexto;

class AreaGraficoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.areagrafico.index')->with('textos', GraficoTexto::get())
																		 ->with('arquivos', GraficoArquivo::orderBy('titulo', 'ASC')->get());
	}

	public function detalhes($slug)
	{
		$this->layout->with('css', 'css/areagrafico');
		$this->layout->content = View::make('frontend.areagrafico.texto')->with('texto', GraficoTexto::where('slug', '=', $slug)->first());
	}
}
