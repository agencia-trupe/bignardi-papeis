<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \ProdutosCertificacoes;

class ProdutosCertificacoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtoscertificacoes.index')->with('registros', ProdutosCertificacoes::ordenado());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtoscertificacoes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProdutosCertificacoes;

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 60, null, 'produtoscertificacoes/');
		if($imagem) $object->imagem = $imagem;
	
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação criado com sucesso.');
			return Redirect::route('painel.produtoscertificacoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Certificação!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtoscertificacoes.edit')->with('registro', ProdutosCertificacoes::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProdutosCertificacoes::find($id);

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 60, null, 'produtoscertificacoes/');
		if($imagem) $object->imagem = $imagem;
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação alterado com sucesso.');
			return Redirect::route('painel.produtoscertificacoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Certificação!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosCertificacoes::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Certificação removido com sucesso.');

		return Redirect::route('painel.produtoscertificacoes.index');
	}

}