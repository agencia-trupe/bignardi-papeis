<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Sustentabilidade;

class SustentabilidadeController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '5';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.sustentabilidade.index')->with('registros', Sustentabilidade::orderBy('ordem', 'ASC')->get())
																			 ->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.sustentabilidade.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Sustentabilidade;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$t=date('dmYHis');
		$imagem = Thumb::make('imagem', 480, 480, 'sustentabilidade/', $t);
		Thumb::make('imagem', 235, 235, 'sustentabilidade/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof(Sustentabilidade::all()) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.sustentabilidade.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.sustentabilidade.edit')->with('registro', Sustentabilidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Sustentabilidade::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->slug = Str::slug($object->id.' '.$object->titulo);
			
		
		$t=date('dmYHis');
		$imagem = Thumb::make('imagem', 480, 480, 'sustentabilidade/', $t);
		Thumb::make('imagem', 235, 235, 'sustentabilidade/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.sustentabilidade.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Sustentabilidade::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.sustentabilidade.index');
	}

}