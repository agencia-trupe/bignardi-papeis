<?php

namespace Painel;

// Core
use \Request, \Input, \DB;

// Models
use \Cor;
// BaseClass
use \Controller;

class AjaxController extends Controller {

	public function __construct(){

	}

	/**
	 * Atualiza a ordem dos itens da tabela
	 *
	 * @return void
	 */
	function gravaOrdem(){
		/* Só aceita requisições feitas via AJAX */
		if(Request::ajax()){
			$menu = Input::get('data');
			$tabela = Input::get('tabela');

	        for ($i = 0; $i < count($menu); $i++) {
	        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
	        }
	        return json_encode($menu);
    	}else{
    		return "no-ajax";
    	}
	}

	function adicionarCor(){
		if(Request::ajax()){
			$titulo = Input::get('tit');
			$hexa = Input::get('hex');

			if (strpos($hexa, '#') !== 0) {
				$hexa = "#".$hexa;
			}

			if(sizeof(Cor::where('titulo', '=', $titulo)->first()) > 0){
				$retorno['mensagem'] = "Título de cor já cadastrado";
	        	$retorno['erro'] = 1;
			}else{

				if(sizeof(Cor::where('hex', '=', $hexa)->first()) > 0){
					$retorno['mensagem'] = "Código de cor já cadastrado";
		        	$retorno['erro'] = 1;
				}else{

			        $cor = new Cor;
			        $cor->titulo = $titulo;
			        $cor->hex = $hexa;
			        $retorno = array();

			        if($cor->save()){
			        	$retorno['mensagem'] = "<label style='width: 30%;'><input type='checkbox'  name='cores[]'' value='".$cor->id."'> <span style='margin:0; width:10px; height:10px; display:inline-block; border-radius:100%; background:".$cor->hex."; border:1px #ccc solid;'></span> ".$cor->titulo."</label>";
			        	$retorno['erro'] = 0;
			        }else{
			        	$retorno['mensagem'] = "erro ao cadastrar";
			        	$retorno['erro'] = 1;
			        }
		    	}
	    	}

	        return json_encode($retorno);
    	}else{
    		return "no-ajax";
    	}	
	}
}
