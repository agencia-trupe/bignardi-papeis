<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \GraficoTexto, \GraficoTextoItem;

class GraficoTextosItensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$texto_id = Input::get('texto_id');

		$texto = GraficoTexto::find($texto_id);

		if(!$texto_id || !$texto) return Redirect::back();

		$this->layout->content = View::make('backend.graficotextositens.index')->with('registros', $texto->itens)
																			   ->with('texto', $texto);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$texto_id = Input::get('texto_id');

		$texto = GraficoTexto::find($texto_id);

		if(!$texto_id || !$texto) return Redirect::back();

		$this->layout->content = View::make('backend.graficotextositens.form')->with('texto', $texto);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new GraficoTextoItem;

		$object->grafico_textos_id = Input::get('grafico_textos_id');
		$object->titulo = Input::get('titulo');
		$object->descricao = Input::get('descricao');

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.graficotextositens.index', array('texto_id' => $object->grafico_textos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$item = GraficoTextoItem::find($id);
		$texto = GraficoTexto::find($item->grafico_textos_id);

		if(!$item || !$texto) return Redirect::back();

		$this->layout->content = View::make('backend.graficotextositens.edit')->with('registro', $item)
																			  ->with('texto', $texto);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = GraficoTextoItem::find($id);

		$object->titulo = Input::get('titulo');
		$object->descricao = Input::get('descricao');
		$object->slug = Str::slug($object->id.' '.$object->titulo);
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.graficotextositens.index', array('texto_id' => $object->grafico_textos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = GraficoTextoItem::find($id);
		$texto_id = $object->grafico_textos_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.graficotextositens.index', array('texto_id' => $texto_id));
	}

}