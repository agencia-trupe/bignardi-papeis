<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \GraficoArquivo;

class GraficoArquivosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.graficoarquivos.index')->with('registros', GraficoArquivo::ordenado()->paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.graficoarquivos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new GraficoArquivo;

		$object->titulo = Input::get('titulo');
		$object->descricao = Input::get('descricao');

		if(Input::hasFile('arquivo')){
			$arquivo = Input::file('arquivo');
			$arquivo->move('assets/files/graficoarquivos', Str::slug($arquivo->getClientOriginalName()));
			$object->arquivo = Str::slug($arquivo->getClientOriginalName());
		}

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Arquivo criado com sucesso.');
			return Redirect::route('painel.graficoarquivos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('arquivo'));
			return Redirect::back()->withErrors(array('Erro ao criar Arquivo!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.graficoarquivos.edit')->with('registro', GraficoArquivo::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = GraficoArquivo::find($id);

		$object->titulo = Input::get('titulo');
		$object->descricao = Input::get('descricao');

		if(Input::hasFile('arquivo')){
			$arquivo = Input::file('arquivo');
			$arquivo->move('assets/files/graficoarquivos', Str::slug($arquivo->getClientOriginalName()));
			$object->arquivo = Str::slug($arquivo->getClientOriginalName());
		}

		$object->slug = Str::slug($object->id.' '.$object->titulo);			

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Arquivo alterado com sucesso.');
			return Redirect::route('painel.graficoarquivos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('arquivo'));
			return Redirect::back()->withErrors(array('Erro ao criar Arquivo!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = GraficoArquivo::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Arquivo removido com sucesso.');

		return Redirect::route('painel.graficoarquivos.index');
	}

}