<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Produto, \Categoria, \Cor, \ProdutosCertificacoes;

class ProdutosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.produtos.index')->with('registros', Produto::ordenado())
																	 ->with('categorias', Categoria::ordenado());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtos.form')->with('categorias', Categoria::ordenado())
																	->with('cores', Cor::ordenado())
																	->with('certificacoes', ProdutosCertificacoes::ordenado());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Produto;

		$object->categorias_id = Input::get('categorias_id');

		$object->titulo = Input::get('titulo');
		$object->titulo_en = Input::get('titulo_en');
		$object->titulo_es = Input::get('titulo_es');

		$object->tipo = Input::get('tipo');
		$object->tipo_en = Input::get('tipo_en');
		$object->tipo_es = Input::get('tipo_es');

		$object->descricao = Input::get('descricao');
		$object->descricao_en = Input::get('descricao_en');
		$object->descricao_es = Input::get('descricao_es');

		$object->tipo_impressao = Input::get('tipo_impressao');
		$object->tipo_impressao_en = Input::get('tipo_impressao_en');
		$object->tipo_impressao_es = Input::get('tipo_impressao_es');

		$object->aplicacoes = Input::get('aplicacoes');
		$object->aplicacoes_en = Input::get('aplicacoes_en');
		$object->aplicacoes_es = Input::get('aplicacoes_es');

		$object->ph = Input::get('ph');
		$object->ph_en = Input::get('ph_en');
		$object->ph_es = Input::get('ph_es');

		$object->gramatura = Input::get('gramatura');
		$object->gramatura_en = Input::get('gramatura_en');
		$object->gramatura_es = Input::get('gramatura_es');

		$object->folhas = Input::get('folhas');
		$object->folhas_en = Input::get('folhas_en');
		$object->folhas_es = Input::get('folhas_es');

		$object->bobinas = Input::get('bobinas');
		$object->bobinas_en = Input::get('bobinas_en');
		$object->bobinas_es = Input::get('bobinas_es');

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 330, 220, 'produtos/thumbs/', $t);
		Thumb::make('imagem', 900, null, 'produtos/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();


			if(Input::has('cores'))
				$object->cores()->sync(Input::get('cores'));

			if(Input::has('certificacoes'))
				$object->certificacoes()->sync(Input::get('certificacoes'));

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto criado com sucesso.');
			return Redirect::route('painel.produtos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Produto!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtos.edit')->with('registro', Produto::find($id))
																	->with('categorias', Categoria::ordenado())
																	->with('cores', Cor::ordenado())
																	->with('certificacoes', ProdutosCertificacoes::ordenado());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Produto::find($id);

		$object->categorias_id = Input::get('categorias_id');

		$object->titulo = Input::get('titulo');
		$object->titulo_en = Input::get('titulo_en');
		$object->titulo_es = Input::get('titulo_es');

		$object->tipo = Input::get('tipo');
		$object->tipo_en = Input::get('tipo_en');
		$object->tipo_es = Input::get('tipo_es');

		$object->descricao = Input::get('descricao');
		$object->descricao_en = Input::get('descricao_en');
		$object->descricao_es = Input::get('descricao_es');

		$object->tipo_impressao = Input::get('tipo_impressao');
		$object->tipo_impressao_en = Input::get('tipo_impressao_en');
		$object->tipo_impressao_es = Input::get('tipo_impressao_es');

		$object->aplicacoes = Input::get('aplicacoes');
		$object->aplicacoes_en = Input::get('aplicacoes_en');
		$object->aplicacoes_es = Input::get('aplicacoes_es');

		$object->ph = Input::get('ph');
		$object->ph_en = Input::get('ph_en');
		$object->ph_es = Input::get('ph_es');

		$object->gramatura = Input::get('gramatura');
		$object->gramatura_en = Input::get('gramatura_en');
		$object->gramatura_es = Input::get('gramatura_es');

		$object->folhas = Input::get('folhas');
		$object->folhas_en = Input::get('folhas_en');
		$object->folhas_es = Input::get('folhas_es');

		$object->bobinas = Input::get('bobinas');
		$object->bobinas_en = Input::get('bobinas_en');
		$object->bobinas_es = Input::get('bobinas_es');

		$object->slug = Str::slug($object->id.' '.$object->titulo);

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 330, 220, 'produtos/thumbs/', $t);
		Thumb::make('imagem', 900, null, 'produtos/', $t);
		if($imagem) $object->imagem = $imagem;

		$cores = Input::has('cores') ? Input::get('cores') : array('0');
		$object->cores()->sync($cores);

		$certs = Input::has('certificacoes') ? Input::get('certificacoes') : array('0');
		$object->certificacoes()->sync($certs);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Produto alterado com sucesso.');
			return Redirect::route('painel.produtos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao alterar Produto! '.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Produto::find($id);
		$object->cores()->sync(array());
		$object->certificacoes()->sync(array());
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Produto removido com sucesso.');

		return Redirect::route('painel.produtos.index');
	}

}