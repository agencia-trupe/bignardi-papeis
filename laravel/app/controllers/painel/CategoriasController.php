<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Categoria;

class CategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '6';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.categorias.index')->with('registros', Categoria::orderBy('ordem', 'ASC')->get())
																	   ->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.categorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Categoria;

		$object->titulo = Input::get('titulo');
		$imagem = Thumb::make('imagem', 300, 215, 'categorias/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( Categoria::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criada com sucesso.');
			return Redirect::route('painel.categorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.categorias.edit')->with('registro', Categoria::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Categoria::find($id);

		$object->titulo = Input::get('titulo');
		$object->titulo_en = Input::get('titulo_en');
		$object->titulo_es = Input::get('titulo_es');
		$imagem = Thumb::make('imagem', 300, 215, 'categorias/');
		if($imagem) $object->imagem = $imagem;

		$object->slug = Str::slug($object->id.' '.$object->titulo);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterada com sucesso.');
			return Redirect::route('painel.categorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Categoria::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.categorias.index');
	}

}