<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Cor;

class CoresController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cores.index')->with('registros', Cor::ordenado());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cores.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Cor;

		$object->titulo = Input::get('titulo');
		$object->titulo_en = Input::get('titulo_en');
		$object->titulo_es = Input::get('titulo_es');
		$object->hex = Input::get('hex');

		if (strpos($object->hex, '#') !== 0) {
			$object->hex = "#".$object->hex;
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cor criado com sucesso.');
			return Redirect::route('painel.cores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cor!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cores.edit')->with('registro', Cor::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Cor::find($id);

		$object->titulo = Input::get('titulo');
		$object->titulo_en = Input::get('titulo_en');
		$object->titulo_es = Input::get('titulo_es');
		$object->hex = Input::get('hex');

		if (strpos($object->hex, '#') !== 0) {
			$object->hex = "#".$object->hex;
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cor alterado com sucesso.');
			return Redirect::route('painel.cores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cor!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cor::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cor removido com sucesso.');

		return Redirect::route('painel.cores.index');
	}

}