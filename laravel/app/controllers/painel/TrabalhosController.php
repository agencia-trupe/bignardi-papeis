<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Trabalho;

class TrabalhosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$aprovados = Input::get('aprovado');
		$registros = ($aprovados) ? Trabalho::aprovados()->paginate(25) : Trabalho::aguardando()->paginate(25);
		$this->layout->content = View::make('backend.trabalhos.index')->with('registros', $registros)
																	  ->with('aprovados', $aprovados);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.trabalhos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Trabalho;

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');
		$object->telefone = Input::get('telefone');
		$object->empresa = Input::get('empresa');
		$object->legenda = Input::get('legenda');

		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 200, 'trabalhos/thumbs/', $t);
		Thumb::make('imagem', 2000, null, 'trabalhos/', $t);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Trabalho criado com sucesso.');
			return Redirect::route('painel.trabalhos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Trabalho!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.trabalhos.edit')->with('registro', Trabalho::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Trabalho::find($id);

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');
		$object->telefone = Input::get('telefone');
		$object->empresa = Input::get('empresa');
		$object->legenda = Input::get('legenda');
		$object->aprovado = Input::get('aprovado');

		if($object->aprovado == '1'){
			$object->data_aprovacao = Date('Y-m-d');
		}
		
		$t=date('YmdHis');
		$imagem = Thumb::make('imagem', 200, 200, 'trabalhos/thumbs/', $t);
		Thumb::make('imagem', 2000, null, 'trabalhos/', $t);
		if($imagem) $object->imagem = $imagem;
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Trabalho alterado com sucesso.');
			return Redirect::route('painel.trabalhos.index', array('aprovado' => $object->aprovado));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Trabalho!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Trabalho::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Trabalho removido com sucesso.');

		return Redirect::route('painel.trabalhos.index');
	}

}