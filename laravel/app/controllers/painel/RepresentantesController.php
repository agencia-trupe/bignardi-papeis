<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Local, \Cidade, \Estado, \Representante;

class RepresentantesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$estado = Input::get('filtra-estado');
		
		$locaisArr = array('estado' => $estado);

		if($estado){
			$listaRepresentantes = \DB::table('representantes')->join('rep_cidades_representantes', 'representantes.id', '=', 'rep_cidades_representantes.representante_id')
															  ->where('rep_cidades_representantes.estado_id', $estado)
															  ->select('representantes.*')
															  ->paginate(25);
		}else{
			$listaRepresentantes = Representante::paginate(25);
		}

		$this->layout->content = View::make('backend.representantes.index')->with('registros', $listaRepresentantes)
																			->with('estados', Estado::orderBy('nome')->get())
																			->with('filtra_estado', $estado);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.representantes.form')->with('estados', Estado::orderBy('nome')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Representante;

		$object->regiao = Input::get('regiao');
		$object->empresa = Input::get('empresa');
		$object->texto = Input::get('texto');
		$object->email = Input::get('email');
		$object->cep = Input::get('cep');

		try {

			$object->save();

			$estados = Input::get('representantes_estado_id');
			
			foreach ($estados as $key => $value) {
				if($value){
					$local = new Local;
					$local->estado_id = $value;
					$local->representante_id = $object->id;
					$local->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Representante criado com sucesso.');
			return Redirect::route('painel.representantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			//return Redirect::back()->withErrors(array('Erro ao adicionar Representante!'));
			return Redirect::back()->withErrors(array($e->getMessage()));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.representantes.edit')->with('registro', Representante::find($id))
																		  ->with('locais', Local::where('representante_id','=',$id)->get())
																		  ->with('estados', Estado::orderBy('nome')->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Representante::find($id);

		$object->regiao = Input::get('regiao');
		$object->empresa = Input::get('empresa');
		$object->texto = Input::get('texto');
		$object->email = Input::get('email');
		$object->cep = Input::get('cep');

		try {

			$object->save();

			$estados = Input::get('representantes_estado_id');
			
			if(Input::has('representantes_estado_id')){
				Local::where('representante_id', '=', $object->id)->delete();
				foreach ($estados as $key => $value) {
					if($value){
						$local = new Local;
						$local->estado_id = $value;
						$local->representante_id = $object->id;
						$local->save();
					}
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Representante alterado com sucesso.');
			return Redirect::route('painel.representantes.index');			

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Representante!'));			
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Representante::find($id);
		$object->delete();

		\Local::where('representante_id', '=', $id)->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Representante removido com sucesso.');

		return Redirect::route('painel.representantes.index');
	}

}