<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Certificacao;

class CertificacoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.certificacoes.index')->with('registros', Certificacao::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.certificacoes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Certificacao;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 150, null, 'certificacoes/', false, '#FFFFFF', false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação criada com sucesso.');
			return Redirect::route('painel.certificacoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Certificação!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.certificacoes.edit')->with('registro', Certificacao::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Certificacao::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 150, null, 'certificacoes/', false, '#FFFFFF', false);
		if($imagem) $object->imagem = $imagem;
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação alterada com sucesso.');
			return Redirect::route('painel.certificacoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Certificação!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Certificacao::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Certificação removido com sucesso.');

		return Redirect::route('painel.certificacoes.index');
	}

}