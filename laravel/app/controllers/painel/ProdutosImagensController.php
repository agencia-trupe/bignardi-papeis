<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, ProdutosImagens, \Produto;

class ProdutosImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$id_produtos = Input::get('id_produtos');

		$produto = Produto::find($id_produtos);

		if(is_null($produto)) return Redirect::route('painel.produtos.index');

		$this->layout->content = View::make('backend.produtosimagens.index')->with('produto', $produto);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$id_produtos = Input::get('id_produtos');

		$produto = Produto::find($id_produtos);

		if(is_null($produto)) return Redirect::route('painel.produtos.index');

		$this->layout->content = View::make('backend.produtosimagens.form')->with('produto', $produto);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProdutosImagens;

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 900, null, 'produtosimagens/', $t);
		Thumb::make('imagem', 60, 60, 'produtosimagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;

		$object->id_produtos = Input::get('id_produtos');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('id_produtos' => $object->id_produtos));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$imagem = ProdutosImagens::find($id);

		$produto = $imagem->produto;

		if(is_null($produto)) return Redirect::route('painel.produtos.index');

		$this->layout->content = View::make('backend.produtosimagens.edit')->with('registro', ProdutosImagens::find($id))
																		   ->with('produto', $produto);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProdutosImagens::find($id);

		$t=Date('YmdHis');
		$imagem = Thumb::make('imagem', 900, null, 'produtosimagens/', $t);
		Thumb::make('imagem', 60, 60, 'produtosimagens/thumbs/', $t);
		if($imagem) $object->imagem = $imagem;
		
		$object->id_produtos = Input::get('id_produtos');
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.produtosimagens.index', array('id_produtos' => $object->id_produtos));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosImagens::find($id);
		$id_index = $object->id_produtos;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.produtosimagens.index', array('id_produtos' => $id_index));
	}

}