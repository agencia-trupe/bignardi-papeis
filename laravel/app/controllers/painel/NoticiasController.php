<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \File, \Noticia, \Tools;

class NoticiasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.noticias.index')->with('registros', Noticia::ordenado()->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.noticias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Noticia;

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 590, null, 'noticias/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Notícia criada com sucesso.');
			return Redirect::route('painel.noticias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Notícia!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.noticias.edit')->with('registro', Noticia::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Noticia::find($id);

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		
		if(Input::has('remover_imagem') && Input::get('remover_imagem') == 1){
			$object->imagem = null;
		}

		$imagem = Thumb::make('imagem', 590, null, 'noticias/');
		if($imagem) $object->imagem = $imagem;

		$object->slug = Str::slug($object->id.' '.$object->titulo);
			
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Notícia alterada com sucesso.');
			return Redirect::route('painel.noticias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Notícia!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Noticia::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Notícia removida com sucesso.');

		return Redirect::route('painel.noticias.index');
	}

}