<?php

use \Contato;
use \Categoria;
use \Produto;

class EnglishController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function quem_somos()
	{
		$this->layout->content = View::make('frontend.english.quem_somos');
	}

	public function certificacoes()
	{
		$this->layout->with('css', 'css/certificacoes');
		$this->layout->content = View::make('frontend.english.certificacoes');
	}

	public function contato()
	{
		$this->layout->with('css', 'css/contato');
		$this->layout->content = View::make('frontend.english.contato')->with('contato', Contato::first());
	}

	public function produtos()
	{
		$this->layout->with('css', 'css/produtos');
		$this->layout->content = View::make('frontend.english.produtos')->with('categorias', Categoria::ordenado());
	}

	public function produtos_detalhes($slug)
	{
		$produto = Produto::where('slug', '=', $slug)->first();

		$outros = Produto::where('categorias_id', '=', $produto->categorias_id)->where('id', '!=', $produto->id)->orderBy('ordem', 'ASC')->get();

		$this->layout->with('css', 'css/produtos');
		$this->layout->content = View::make('frontend.english.produtos_detalhes')->with('categorias', Categoria::ordenado())
																		 		 ->with('produto', $produto)
																		 		 ->with('outros', $outros);
	}
}
