<?php

use Noticia as Noticia;
use Banner as Banner;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('noticias', Noticia::ordenado()->limit(3)->get())
																  ->with('banners_reverso', Banner::orderBy('ordem', 'DESC')->get())
																  ->with('banners', Banner::orderBy('ordem', 'ASC')->get())
																  ->with('banners_mobile', Banner::orderBy('ordem', 'ASC')->limit(3)->get());
	}

}
