<?php

use \Produto, \Categoria;

class ProdutosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.produtos.index')->with('categorias', Categoria::ordenado());
	}

	public function detalhes($slug)
	{
		$produto = Produto::where('slug', '=', $slug)->first();

		$outros = Produto::where('categorias_id', '=', $produto->categorias_id)->where('id', '!=', $produto->id)->orderBy('ordem', 'ASC')->get();

		$this->layout->with('css', 'css/produtos');
		$this->layout->content = View::make('frontend.produtos.detalhes')->with('categorias', Categoria::ordenado())
																		 ->with('produto', $produto)
																		 ->with('outros', $outros);
	}
}
