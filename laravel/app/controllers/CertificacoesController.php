<?php

use \Certificacao;

class CertificacoesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.certificacoes.index')->with('certificacoes', Certificacao::orderBy('ordem', 'ASC')->get());
	}

}
