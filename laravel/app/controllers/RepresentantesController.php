<?php

use \Representante, \Estado;

class RepresentantesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug = false)
	{
		if(Input::has('estado_post')){
			$slug = strtolower(Input::get('estado_post'));
		}

		if($slug){
			$estadoSelecionado = Estado::where('uf', '=', $slug)->first();

			if(sizeof($estadoSelecionado) == 0) App::abort('404');

			$representantes = \DB::table('representantes')->join('rep_cidades_representantes', 'representantes.id', '=', 'rep_cidades_representantes.representante_id')
														  ->where('rep_cidades_representantes.estado_id', $estadoSelecionado->id)
														  ->select('representantes.*')
														  ->get();
		}else{
			$representantes = false;
		}

		$this->layout->content = View::make('frontend.representantes.index')->with('listaEstados', Estado::orderBy('uf')->get())
																			->with('marcarEstado', $slug)
																			->with('listaRepresentantes', $representantes);
	}

}
