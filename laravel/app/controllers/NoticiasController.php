<?php

use \Noticia;

class NoticiasController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug = false)
	{
		if($slug){
			$detalhe = Noticia::where('slug', '=', $slug)->first();
		}else{
			$detalhe = Noticia::ordenado()->first();
		}

		$this->layout->content = View::make('frontend.noticias.index')->with('listaNoticias', Noticia::ordenado()->get())
																	  ->with('detalhe', $detalhe);
	}

}
