<?php

use \Trabalho;

class TrabalhosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.trabalho.index')->with('trabalhos', Trabalho::aprovados()->paginate(12));
	}

	public function enviar()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');
		$telefone = Input::get('telefone');
		$empresa = Input::get('empresa');
		$legenda = Input::get('legenda');

		$t=date('YmdHis');
		$arquivo = Thumb::make('arquivo', 200, 200, 'trabalhos/thumbs/', $t);
		Thumb::make('arquivo', 2000, null, 'trabalhos/', $t);

		$validacao = true;

		if(!$nome){
			$validacao = false;
			$mensagem = "O nome é obrigatório";
		}

		if(!$email && $validacao){
			$validacao = false;
			$mensagem = "O e-mail é obrigatório";
		}

		if(!$legenda && $validacao){
			$validacao = false;
			$mensagem = "A legenda do trabalho é obrigatória";
		}

		if(!$arquivo && $validacao){
			$validacao = false;
			$mensagem = "Selecione um arquivo";
		}

		if(!$validacao){
			Session::flash('envio-nome', $nome);
			Session::flash('envio-email', $email);
			Session::flash('envio-telefone', $telefone);
			Session::flash('envio-empresa', $empresa);
			Session::flash('envio-legenda', $legenda);
			Session::flash('erro', $mensagem);
			return Redirect::back();
		}else{
			$registro = new Trabalho;
			$registro->nome = $nome;
			$registro->email = $email;
			$registro->telefone = $telefone;
			$registro->empresa = $empresa;
			$registro->legenda = $legenda;
			$registro->imagem = $arquivo;
			$registro->save();
			Session::flash('enviado', true);
			return Redirect::back();
		}

	}
}
