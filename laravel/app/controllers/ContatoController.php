<?php

use \Contato;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato.index')->with('contato', Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@bignardipapeis.com.br', 'Bignardi Papéis')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}

		Session::flash('enviado', true);
		return Redirect::back();
	}

	public function enviaralternativo()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('rosana.albanez@bignardi.com.br', 'Bignardi Papéis')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}

		Session::flash('enviado', true);
		return Redirect::back();
	}
}
