<?php

use \QuemSomos;

class QuemSomosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.quemsomos.index')->with('quemsomos', QuemSomos::first());
	}

}
