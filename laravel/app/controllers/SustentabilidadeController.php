<?php

use \Sustentabilidade;

class SustentabilidadeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.sustentabilidade.index')->with('sustentabilidade', Sustentabilidade::ordenado());
	}

	public function detalhes($slug = false)
	{
		if(!$slug) return Redirect::to('sustentabilidade');

		$texto = Sustentabilidade::where('slug', '=', $slug)->first();

		$outrosTextos = Sustentabilidade::orderBy('ordem', 'ASC')->where('id', '!=', $texto->id)->get();

		$this->layout->with('css', 'css/sustentabilidade');
		$this->layout->content = View::make('frontend.sustentabilidade.detalhes')->with('sustentabilidade', Sustentabilidade::ordenado())
																			     ->with('texto', $texto)
																			     ->with('outrosTextos', $outrosTextos);
	}
}
