<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));
Route::get('quemsomos', array('as' => 'quemsomos', 'uses' => 'QuemSomosController@index'));
Route::get('certificacoes', array('as' => 'certificacoes', 'uses' => 'CertificacoesController@index'));
Route::any('representantes/{slug?}', array('as' => 'representantes', 'uses' => 'RepresentantesController@index'));
Route::get('noticias/{slug?}', array('as' => 'noticias', 'uses' => 'NoticiasController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));

Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));
Route::post('contato', array('as' => 'contato.enviarAlternativo', 'uses' => 'ContatoController@enviaralternativo'));

Route::get('produtos', array('as' => 'produtos', 'uses' => 'ProdutosController@index'));
Route::get('produtos/{slug?}', array('as' => 'produtos.detalhes', 'uses' => 'ProdutosController@detalhes'));
Route::get('sustentabilidade', array('as' => 'sustentabilidade', 'uses' => 'SustentabilidadeController@index'));
Route::get('sustentabilidade/{slug?}', array('as' => 'sustentabilidade.detalhes', 'uses' => 'SustentabilidadeController@detalhes'));
Route::get('areagrafico', array('as' => 'areagrafico', 'uses' => 'AreaGraficoController@index'));
Route::get('areagrafico/{slug?}', array('as' => 'areagrafico.detalhes', 'uses' => 'AreaGraficoController@detalhes'));
Route::get('trabalho', array('as' => 'trabalho', 'uses' => 'TrabalhosController@index'));
Route::post('trabalho', array('as' => 'trabalho.enviar', 'uses' => 'TrabalhosController@enviar'));

// Apresentação de produtos e contato em inglês
Route::get('english', array('as' => 'english.quemsomos', 'uses' => 'EnglishController@quem_somos'));
Route::get('english/certifications', array('as' => 'english.certificacoes', 'uses' => 'EnglishController@certificacoes'));
Route::get('english/contact', array('as' => 'english.contato', 'uses' => 'EnglishController@contato'));
Route::get('english/products', array('as' => 'english.produtos', 'uses' => 'EnglishController@produtos'));
Route::get('english/products/{slug?}', array('as' => 'english.produtos.detalhes', 'uses' => 'EnglishController@produtos_detalhes'));

// Apresentação de produtos e contato em espanhol
Route::get('espanol', array('as' => 'espanol.quemsomos', 'uses' => 'EspanolController@quem_somos'));
Route::get('espanol/certificaciones', array('as' => 'espanol.certificacoes', 'uses' => 'EspanolController@certificacoes'));
Route::get('espanol/contacto', array('as' => 'espanol.contato', 'uses' => 'EspanolController@contato'));
Route::get('espanol/productos', array('as' => 'espanol.produtos', 'uses' => 'EspanolController@produtos'));
Route::get('espanol/productos/{slug?}', array('as' => 'espanol.produtos.detalhes', 'uses' => 'EspanolController@produtos_detalhes'));


Route::get('download/{arquivo?}', function($arquivo){

	$pathToFile = 'assets/files/graficoarquivos/'.$arquivo;

	if(!$arquivo || !file_exists($pathToFile)){
		App::abort('404');
	}else{
		return Response::download($pathToFile);
	}
});

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::get('ajax/pegarCidades/{id}', function($id){
	return Response::json(Cidade::where('estado', '=', $id)->get());
});

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));
Route::post('ajax/adicionarCor', array('before' => 'auth', 'uses' => 'Painel\AjaxController@adicionarCor'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('certificacoes', 'Painel\CertificacoesController');
	Route::resource('representantes', 'Painel\RepresentantesController');
	Route::resource('noticias', 'Painel\NoticiasController');
	Route::resource('quemsomos', 'Painel\QuemSomosController');
	Route::resource('sustentabilidade', 'Painel\SustentabilidadeController');
	Route::resource('produtos', 'Painel\ProdutosController');
	Route::resource('categorias', 'Painel\CategoriasController');
	Route::resource('cores', 'Painel\CoresController');
	Route::resource('produtoscertificacoes', 'Painel\ProdutosCertificacoesController');
	Route::resource('graficoarquivos', 'Painel\GraficoArquivosController');
	Route::resource('graficotextos', 'Painel\GraficoTextosController');
	Route::resource('graficotextositens', 'Painel\GraficoTextosItensController');
	Route::resource('trabalhos', 'Painel\TrabalhosController');
Route::resource('produtosimagens', 'Painel\ProdutosImagensController');
//NOVASROTASDOPAINEL//
});
