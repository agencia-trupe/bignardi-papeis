<?php

class ProdutoEnglish extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_english';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'ASC')->get();
    }

    public function imagens()
    {
    	return $this->hasMany('ProdutosImagens', 'id_produtos')->orderBy('ordem', 'asc');
    }

    public function getCoresAttribute($value)
    {
        if(!$value) return null;

        $cores = explode(';', $value);

        foreach($cores as &$c) {
            $c = explode(',', $c);
        }

        return $cores;
    }
}
