<?php

class Produto extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'ASC')->get();
    }

    public function imagens()
    {
    	return $this->hasMany('ProdutosImagens', 'id_produtos')->orderBy('ordem', 'asc');
    }

    public function categoria()
	{
		return $this->belongsTo('Categoria', 'categorias_id');
	}

	public function cores()
	{
		return $this->belongsToMany('Cor');
	}
	
	public function certificacoes()
	{
		return $this->belongsToMany('ProdutosCertificacoes');
	}

	public function coresArray(){
		$todas = \DB::table('cor_produto')->select('cor_id')->where('produto_id', '=', $this->id)->get();
		
		$r = array();
		foreach ($todas as $key => $value) {
			$r[] = $value->cor_id;
		}
		return $r;
	}

	public function certificacoesArray(){
		$todas = \DB::table('produto_produtos_certificacoes')->select('produtos_certificacoes_id')->where('produto_id', '=', $this->id)->get();
		
		$r = array();
		foreach ($todas as $key => $value) {
			$r[] = $value->produtos_certificacoes_id;
		}
		return $r;
	}
}